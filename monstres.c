#include "monstres.h"

void chargerImagesMonstre(Monstre* monMonstre)   //pour loader la sauvegarde
{
	char path[100];
	
	sprintf(path,"./Images/Monstres/%s/%s/stopped0001.bmp",TYPESTRING(monMonstre->type),monMonstre->nom);
	monMonstre->front=SDL_LoadBMP(path);
	SDL_SetColorKey(monMonstre->front, SDL_SRCCOLORKEY, SDL_MapRGB(monMonstre->front->format, CLR));

	sprintf(path,"./Images/Monstres/%s/%s/stopped0005.bmp",TYPESTRING(monMonstre->type),monMonstre->nom);
	monMonstre->back=SDL_LoadBMP(path);
	SDL_SetColorKey(monMonstre->back, SDL_SRCCOLORKEY, SDL_MapRGB(monMonstre->back->format, CLR));
}

Attaque* chargerAttaque(char nom[40], int type)
{
	Attaque* monAttaque = malloc(sizeof(Attaque));

	char path[100];
	sprintf(path,"./Attaques/%s/%s",TYPESTRING(type),nom);

	FILE * f =fopen(path,"r");
	fscanf(f,"puissance:%d\nheal:%d\nprecision:%d\nindice:%d",&monAttaque->puissance,&monAttaque->heal,&monAttaque->precision,&monAttaque->indice_rarete);
	
	strcpy(monAttaque->nom,nom);
	monAttaque->type=type;
	return monAttaque;
}

Attaque** chargerAttaquesType(int type)
{
	char path[100];
	sprintf(path,"./Attaques/%s",TYPESTRING(type));
	DIR * rep = opendir(path);
	Attaque** attaquesType=malloc(NB_MOB_TYPE(type)*sizeof(Attaque));
	int i=0;
	struct dirent * ent; 
    while ((ent = readdir(rep)) != NULL) 
    {     	
	if ((strcmp(ent->d_name,"."))&&(strcmp(ent->d_name,".."))&&(i<NB_ATT_TYPE(type)))
	{	
		*(attaquesType+i) = chargerAttaque(ent->d_name,type);
		i++;
	}
    } 
    closedir(rep);
    return attaquesType;
}

void libererAttaquesType(Attaque** mesAttaques,int type)
{
	int i;
	for(i=0;i<NB_ATT_TYPE(type);i++)
	{
		free(*(mesAttaques+i));
	}
	free(mesAttaques);
}



Attaquaire* chargerAttaquaire()
{
	Attaquaire* monAttaquaire=malloc(sizeof(Attaquaire));
	monAttaquaire->attaques[EAU] = chargerAttaquesType(EAU);
	monAttaquaire->attaques[FEU] = chargerAttaquesType(FEU);
	monAttaquaire->attaques[PLANTE] = chargerAttaquesType(PLANTE);
	monAttaquaire->attaques[NORMAL] = chargerAttaquesType(NORMAL);
	return monAttaquaire;
}

void libererAttaquaire(Attaquaire* monAttaquaire)
{
	int i;
	for(i=0;i<4;i++)
	{
		libererAttaquesType(monAttaquaire->attaques[i],i);
	}
	free(monAttaquaire);
}



Monstre* chargerMonstre(char nom[20], int type)
{
	Monstre* monMonstre = malloc(sizeof(Monstre));

	monMonstre->niveau = 1;

	char path[100];

	sprintf(path,"./Images/Monstres/%s/%s/stopped0001.bmp",TYPESTRING(type),nom);
	monMonstre->front=SDL_LoadBMP(path);
	SDL_SetColorKey(monMonstre->front, SDL_SRCCOLORKEY, SDL_MapRGB(monMonstre->front->format, CLR));

	sprintf(path,"./Images/Monstres/%s/%s/stopped0005.bmp",TYPESTRING(type),nom);
	monMonstre->back=SDL_LoadBMP(path);
	SDL_SetColorKey(monMonstre->back, SDL_SRCCOLORKEY, SDL_MapRGB(monMonstre->back->format, CLR));

	strcpy(monMonstre->nom,nom);
	monMonstre->type=type;
	monMonstre->attaque[0]=chargerAttaque("pichenaude", NORMAL);  //Par défaut, écraser dans la fonction générerMonstre(type) qui prendra des attaques aléatoires.
	monMonstre->attaque[1]=chargerAttaque("pichenaude", NORMAL);
	monMonstre->attaque[2]=chargerAttaque("pichenaude", NORMAL);
	monMonstre->attaque[3]=chargerAttaque("pichenaude", NORMAL);

	sprintf(path,"./Images/Monstres/%s/%s/carac.txt",TYPESTRING(type),nom);
	FILE * f = fopen(path,"r");
	fscanf(f,"vie:%d\nforce:%d\nhabilete:%d\nindice:%d",&(monMonstre->vie),&(monMonstre->force),&(monMonstre->habilete),&(monMonstre->indice_rarete));
	monMonstre->vie_actuelle = monMonstre->vie;
	monMonstre->experience=0;
	return monMonstre;
}

void libererMonstre(Monstre* monMonstre)
{
	SDL_FreeSurface(monMonstre->front);
	SDL_FreeSurface(monMonstre->back);
	//on ne descend pas aux attaques. Elles existent toutes en un unique exemplaire, pointés bcp de fois. On supprime l'Attaquaire de son coté.
	free(monMonstre);
}



Monstre** chargerMonstresType(int type)
{	
	char path[100];
	sprintf(path,"./Images/Monstres/%s",TYPESTRING(type));
	DIR * rep = opendir(path);
	Monstre** monstresType=malloc(NB_MOB_TYPE(type)*sizeof(Monstre));
	int i=0;
	struct dirent * ent; 
    while ((ent = readdir(rep)) != NULL) 
    {     	
	if ((strcmp(ent->d_name,"."))&&(strcmp(ent->d_name,".."))&&(i<NB_MOB_TYPE(type)))
	{	
		*(monstresType+i) = chargerMonstre(ent->d_name,type);
		i++;
	}
    } 
    closedir(rep);
    
    
    Monstre* buffer;                        //tri en place (c'est sur), par insertion (je crois)
    int j;
    for(i=0;i<NB_MOB_TYPE(type);i++)
    {
		buffer=*(monstresType+i);
		j=i-1;
		while ((j>=0)&&(buffer->indice_rarete < (*(monstresType+j))->indice_rarete))
			{
			(*(monstresType+j+1))=(*(monstresType+j));
			j--;
			}
		(*(monstresType+j+1))=buffer;
    }
      
    return monstresType; 
}

void libererMonstresType(Monstre** mesMonstres, int type)
{
	int i;
	for(i=0;i<NB_MOB_TYPE(type);i++)
	{
		libererMonstre(*(mesMonstres+i));
	}
	free(mesMonstres);
}
			


Bestiaire* chargerBestiaire()
{
	Bestiaire* monBestiaire=malloc(sizeof(Bestiaire));
	monBestiaire->monstres[EAU] = chargerMonstresType(EAU);
	monBestiaire->monstres[FEU] = chargerMonstresType(FEU);
	monBestiaire->monstres[NORMAL] = chargerMonstresType(NORMAL);
	monBestiaire->monstres[PLANTE] = chargerMonstresType(PLANTE);
	return monBestiaire;
}

void libererBestiaire(Bestiaire* monBestiaire)
{
	int i;
	for(i=0;i<4;i++)
	{
		libererMonstresType(monBestiaire->monstres[i],i);
	}
	free(monBestiaire);
}



Attaque* chargerAttaqueAlea(int type, Attaquaire* monAttaquaire)
{	
	int tirage = rand_a_b(0,NB_ATT_TYPE(type));
	Attaque** maListe= monAttaquaire->attaques[type];
	return *(maListe+tirage);
}

void genererAttaqueAlea(Monstre* monMonstre, Attaquaire* monAttaquaire)
{
	int type;
	int i;
	int somme;
	
	while( (somme<=0.8*(monMonstre->indice_rarete))||(somme>monMonstre->indice_rarete) )
	{
		somme=0;
		for(i=0;i<4;i++)
		{	
			type = (rand_a_b(0,2)==0) ? monMonstre->type : NORMAL;
			monMonstre->attaque[i]=chargerAttaqueAlea(type, monAttaquaire);
			somme += monMonstre->attaque[i]->indice_rarete;
		}
    }
}


void levelUp(Monstre* monMonstre)
{
	monMonstre->niveau++;

	monMonstre->vie = 1+(monMonstre->vie*1.05);
	monMonstre->force = 1+(monMonstre->force*1.05);
	monMonstre->habilete = 1+(monMonstre->habilete*1.05);
	monMonstre->vie_actuelle = monMonstre->vie;
}

SDL_Surface* copierMobSprite(SDL_Surface* src)  //pour recopier une surface et pas que le pointeur dessus. 
{
	SDL_Surface* newSurface = NULL;
	newSurface = SDL_CreateRGBSurface(SDL_HWSURFACE,MOB_LARGEUR,MOB_HAUTEUR,32,0,0,0,0);
	SDL_FillRect(newSurface, NULL, SDL_MapRGB(newSurface->format, CLR));
	SDL_BlitSurface(src,NULL,newSurface,NULL);
	SDL_SetColorKey(newSurface, SDL_SRCCOLORKEY, SDL_MapRGB(newSurface->format, CLR));
	return newSurface;
}


Monstre* copierMonstre(Monstre* monMonstre)
{
	Monstre* monNouveauMonstre = malloc(sizeof(Monstre));

	monNouveauMonstre->front = copierMobSprite(monMonstre->front);
	monNouveauMonstre->back = copierMobSprite(monMonstre->back);    //Pour pouvoir free de la meme maniere un monstre copié ou pas sans dble free
	
	strcpy(monNouveauMonstre->nom,monMonstre->nom) ;
	monNouveauMonstre->type =monMonstre->type ;
	monNouveauMonstre->niveau =monMonstre->niveau ;
	monNouveauMonstre->vie =monMonstre->vie ;
	monNouveauMonstre->vie_actuelle =monMonstre->vie ;
	monNouveauMonstre->force =monMonstre->force ;
	monNouveauMonstre->habilete =monMonstre->habilete ;
	monNouveauMonstre->indice_rarete =monMonstre->indice_rarete ;
	monNouveauMonstre->attaque[0] =monMonstre->attaque[0] ;
	monNouveauMonstre->attaque[1] =monMonstre->attaque[1] ;
	monNouveauMonstre->attaque[2] =monMonstre->attaque[2] ;
	monNouveauMonstre->attaque[3] =monMonstre->attaque[3] ;
	return monNouveauMonstre;
}



//genere un monstre d'un niveau et d'un type donné, avec un jeu aléatoir d'attaques dépendant de son type et des indices de rarete
Monstre* genererMonstre(int type,int niveau, Bestiaire* monBestiaire, Attaquaire* monAttaquaire)   
{
	int i;
	int somme =0;
	Monstre ** maListe = monBestiaire->monstres[type];
	for(i=0;i<NB_MOB_TYPE(type);i++)
	{
		somme=somme+100-(*(maListe+i))->indice_rarete;
	}
	int tirage = rand_a_b(0,somme+1);
	i=0;
	while (100-((*(maListe+i))->indice_rarete)<tirage)
	{
		tirage -=100-(*(maListe+i))->indice_rarete;
		i++;
	}

	Monstre* monMonstre = copierMonstre(*(maListe +i));
	
	tirage = rand_a_b(-10,11);
	monMonstre->vie *= 1+ (double) tirage/100;
	tirage = rand_a_b(-10,11);
	monMonstre->force *=1+ (double) tirage/100;
	tirage = rand_a_b(-10,11);
	monMonstre->habilete *=1+ (double) tirage/100;

	monMonstre->niveau=0;
	for(i=0;i<niveau;i++)
	{
		levelUp(monMonstre);
	}

	genererAttaqueAlea(monMonstre, monAttaquaire);
	monMonstre->vie_actuelle = monMonstre->vie;

	return monMonstre;
}


