#include "map_deplacement.h"

void deplacer(Carte* maCarte)
{
	int x=maCarte->positionHeros[0];
	int y=maCarte->positionHeros[1];
	switch(maCarte->directionHeros)
	{
	case GAUCHE:
		if ((x>0) && (maCarte->grille[x-1][y]!='A') && (maCarte->grille[x-1][y]!='M') && (maCarte->grille[x-1][y]!='P')&& (maCarte->grille[x-1][y]!='K'))
			(maCarte->positionHeros[0])--;
		break;
	case DROITE:
		if ((x<NB_CASES_LARGEUR-1) && (maCarte->grille[x+1][y]!='A') && (maCarte->grille[x+1][y]!='M') && (maCarte->grille[x+1][y]!='P')&& (maCarte->grille[x+1][y]!='K'))
			(maCarte->positionHeros[0])++;
		break;
	case HAUT:
		if ((y>0) && (maCarte->grille[x][y-1]!='A') && (maCarte->grille[x][y-1]!='M') && (maCarte->grille[x][y-1]!='P')&& (maCarte->grille[x][y-1]!='K'))
			(maCarte->positionHeros[1])--;
		break;
	case BAS:
		if ((y<NB_CASES_HAUTEUR-1) && (maCarte->grille[x][y+1]!='A') && (maCarte->grille[x][y+1]!='M') && (maCarte->grille[x][y+1]!='P')&& (maCarte->grille[x][y+1]!='K'))
			(maCarte->positionHeros[1])++;
		break;
	case STILL:
		break;
	default:
		break;
	}
}

Monstre* testerCarte(Carte* maCarte, Images* mesImages, Parametres* mesParametres, Dresseur* monDresseur)
{
	int x=maCarte->positionHeros[0];
	int y=maCarte->positionHeros[1];
	char Cspawn=' ';
	
	switch(maCarte->grille[x][y])
	{
		case 'H':
			return rencontre(mesParametres, monDresseur);
		case '+':
			if (mesParametres->mode==SAFARI)
			{
				mesParametres->numeroCarte = (mesParametres->numeroCarte)%8+1;
				mesParametres->numeroTheme = (mesParametres->numeroTheme)%8+1;
			}
			if (mesParametres->mode==DONJON)
			{
				mesParametres->numeroCarte = rand_a_b(1,NB_CARTES+1);
				mesParametres->numeroTheme = rand_a_b(1,NB_THEMES+1);
				(mesParametres->difficulte)++;
			}
			Cspawn = 'I';
			break;
		case '-':
			if (mesParametres->mode==SAFARI)
			{
				mesParametres->numeroCarte = mesParametres->numeroCarte-1+(mesParametres->numeroCarte==1)*8;
				mesParametres->numeroTheme = mesParametres->numeroTheme-1+(mesParametres->numeroTheme==1)*8;
			}
			if (mesParametres->mode==DONJON)
			{
				mesParametres->numeroCarte = rand_a_b(1,NB_CARTES+1);
				mesParametres->numeroTheme = rand_a_b(1,NB_THEMES+1);
				(mesParametres->difficulte--);
			}
			Cspawn='O';
			break;
		case 'C':
			mesParametres->numeroCarte = 0;
			mesParametres->numeroTheme = 0;
			mesParametres->mode = CENTRE;
			Cspawn = 'I';
			break;
		case 'S':
			mesParametres->numeroCarte = 1;
			mesParametres->numeroTheme =1;
			mesParametres->mode = SAFARI;
			Cspawn = 'I';
			
			break;
		case 'D':
			if(isAlive(monDresseur))
			{
				mesParametres->numeroCarte = rand_a_b(1,NB_CARTES+1);
				mesParametres->numeroTheme = rand_a_b(1,NB_THEMES+1);
				mesParametres->difficulte = 1;
				mesParametres->mode = DONJON;
				Cspawn = 'I';
			}
			break;
		case 'm':
			if(maCarte->directionHeros==HAUT)
			{
				mesParametres->fenetre = MAGASIN;
			}
			break;
		case '1':
			if(maCarte->directionHeros==HAUT)
			{
				mesParametres->fenetre = AIDE1;
			}
			break;
		case '2':
			if(maCarte->directionHeros==HAUT)
			{
				mesParametres->fenetre = AIDE2;
			}
			break;
		case 'R':
			mesParametres->fenetre = HEAL;
			break;
		case 'k':
			if(maCarte->directionHeros==HAUT)
			{
				mesParametres->fenetre = FIGHT;
			}
			break;
		default:
			break;
	}
	if(mesParametres->difficulte>9)
	{
		mesParametres->numeroCarte=9;
		mesParametres->numeroTheme=0;
	}
	if (Cspawn!= ' ')
	{
		modifierCarte(mesParametres->numeroCarte, maCarte);
		spawn(maCarte,Cspawn);
		
		modifierImages(mesParametres->numeroTheme,mesImages);
	}
	return NULL;	
}
			
void spawn(Carte* maCarte,char c)
{
	int continuer = 1;
	int i=0;
	char caseCourante;

	while((continuer)&&(i<NB_CASES_LARGEUR * NB_CASES_HAUTEUR))
	{
		caseCourante = maCarte->grille[i%NB_CASES_LARGEUR][i/NB_CASES_LARGEUR];
		if ((caseCourante=='0')||(caseCourante=='H'))
		{
			maCarte->positionHeros[0] =i%NB_CASES_LARGEUR;
			maCarte->positionHeros[1] =i/NB_CASES_LARGEUR;
		}
		if (caseCourante==c)
		{
			maCarte->positionHeros[0] =i%NB_CASES_LARGEUR;
			maCarte->positionHeros[1] =i/NB_CASES_LARGEUR;
			continuer = 0;
		}
		i++;
	}
}

Monstre* rencontre(Parametres* mesParametres, Dresseur* monDresseur)
{
	int r = rand_a_b(0,100);
	if (r>12)
	{
		return NULL;
	}
	else
	{

		int type1=(mesParametres->numeroTheme-1)%4;
		int type2=(((mesParametres->numeroTheme)/4)%2)*type1;  //vaut 0=NORMAL ou type1. indépendament de type1
		r = rand_a_b(0,2);
		int type = (r==0)?type1:type2;

		Monstre* monMonstre = genererMonstre(type,monDresseur->niveau*(1+0.03*mesParametres->difficulte*(mesParametres->mode==DONJON)), mesParametres->monBestiaire, mesParametres->monAttaquaire);

		return monMonstre;
	}
}
























