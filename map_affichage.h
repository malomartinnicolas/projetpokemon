#ifndef DEF_MAP_AFF
#define DEF_MAP_AFF
	#include "map_constantes.h"	
	#include "monstres.h"
	#include "map_deplacement.h"

	Images * creerImages ();
	void modifierImages(int i,Images* mesImages);
	void libererImages(Images* mesImages);

	Carte * creerCarte ();
	void modifierCarte(int i,Carte* maCarte);
	void afficherCarte(SDL_Surface* ecran,Images* mesImages,SDL_Surface* herosActuel,Carte * maCarte);
	void apparition(SDL_Surface* ecran, Monstre* unMonstre);
	
#endif
