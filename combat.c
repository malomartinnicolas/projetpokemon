#include "combat.h"
const SDL_Color textColor = {0, 0, 0};

int a_joue  = 0;
int perdu = 0;

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination)
{
    SDL_Rect offset;

    offset.x = x;
    offset.y = y;
    
    SDL_FillRect(source, &offset, SDL_MapRGB(source->format, 0, 0, 0));
    
    SDL_BlitSurface( source, NULL, destination, &offset );
}

void afficherTexte(SDL_Surface* ecran, int x, int y, int w, int h, char message[50], int info)
{
	SDL_Rect rect;
	SDL_Surface* surface = NULL;
	SDL_Event event;
	int continuer = 1;

	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;

	SDL_FillRect(ecran, &rect, SDL_MapRGB(ecran->format, 255, 255, 255));

	TTF_Font* font = TTF_OpenFont("Aller_Lt.ttf", 24);
	
	surface = TTF_RenderText_Solid(font, message, textColor);
	
	apply_surface(x+20, y+20, surface, ecran);
	
	if(info == 1)
	{
		SDL_Flip(ecran);
	
		while(continuer)
		{
			SDL_WaitEvent(&event);
				switch(event.type)
				{
					case SDL_QUIT:
						continuer = 0;
						break;
					case SDL_KEYDOWN:
						switch(event.key.keysym.sym)
						{
							case SDLK_ESCAPE:
								continuer = 0;
								break;
							case SDLK_RETURN:
								continuer = 0;
								break;
						}
						break;
				}
		}
		SDL_FillRect(ecran, &rect, SDL_MapRGB(ecran->format, 0, 0, 0));
	}
	SDL_FreeSurface(surface);
	TTF_CloseFont(font);
}

void afficherMonstre2(SDL_Surface* ecran, Monstre* monMonstre)
{
	SDL_Rect mob;

	mob.x = 250;
	mob.y = 400;
	mob.w = 192;
	mob.h = 192;

	SDL_FillRect(ecran, &mob, SDL_MapRGB(ecran->format, 0, 0, 0));

	apply_surface(250, 400, monMonstre->back, ecran);
}

void afficherStats(SDL_Surface* ecran, Monstre* monMonstre, Monstre* monstre)
{
	SDL_Rect stats;
	SDL_Surface* nom = NULL;
	SDL_Surface* hp = NULL;
	char vie[15];
	char temp[30];

	stats.x = 30;
	stats.y = 450;
	stats.w = 250;
	stats.h = 100;

	TTF_Font* font = TTF_OpenFont("Aller_Lt.ttf", 24);

	SDL_FillRect(ecran, &stats, SDL_MapRGB(ecran->format, 255, 255, 255));

	stats.x = 650;
	stats.y = 50;

	SDL_FillRect(ecran, &stats, SDL_MapRGB(ecran->format, 255, 255, 255));

	if(monMonstre->vie_actuelle > monMonstre->vie) monMonstre->vie_actuelle = monMonstre->vie;

	if(monMonstre->vie_actuelle < 0) monMonstre->vie_actuelle = 0;

	if(monstre->vie_actuelle > monstre->vie) monstre->vie_actuelle = monstre->vie;

	if(monstre->vie_actuelle < 0) monstre->vie_actuelle = 0;

	sprintf(vie, "%d / %d", monMonstre->vie_actuelle, monMonstre->vie);
	sprintf(temp, "%s   lvl %d", monMonstre->nom, monMonstre->niveau);

	nom = TTF_RenderText_Solid(font, temp, textColor);
	hp = TTF_RenderText_Solid(font, vie, textColor);

	apply_surface(50, 470, nom, ecran);
	apply_surface(50, 510, hp, ecran);
	

	sprintf(vie, "%d / %d", monstre->vie_actuelle, monstre->vie);
	sprintf(temp, "%s   lvl %d", monstre->nom, monstre->niveau);

	nom = TTF_RenderText_Solid(font, temp, textColor);
	hp = TTF_RenderText_Solid(font, vie, textColor);

	apply_surface(670, 70, nom, ecran);
	apply_surface(670, 110, hp, ecran);
	SDL_FreeSurface(nom);
	SDL_FreeSurface(hp);
}		

Monstre* menuCombat(SDL_Surface* ecran, Dresseur* monDresseur, Monstre* monstre, int nbMonstres, int dresseur)
{
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
	SDL_Rect rectangle;
	SDL_Rect mob;
	SDL_Event event;
	int continuer = 1;
	int pos = 0;
	int argent = 0;
	int choixattaque = 0;
	int xp = 0;
	char messageAttaque[30] = "";

	Monstre* monMonstre = monDresseur->equipe[0];
	int i = 0;

	while(monDresseur->equipe[i]->vie_actuelle == 0 && i<4)
	{
	i++;
	monMonstre = monDresseur->equipe[i];
	}

	int y = 50;

	rectangle.x = 40;
	rectangle.y = 50;
	rectangle.w = 200;
	rectangle.h = 210;

	mob.x = 600;
	mob.y = 170;
	mob.h = 200;
	mob.w = 200;

	SDL_FillRect(ecran, &mob, SDL_MapRGB(ecran->format, 0, 0, 0));

	apply_surface(600, 170, monstre->front, ecran);

	afficherMonstre2(ecran, monMonstre);

	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));
	afficherStats(ecran, monMonstre, monstre);
	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));
	afficherTexte(ecran, 60, 50, 50, 20, "Attaquer", 0);
	afficherTexte(ecran, 60, 100, 50, 20, "Objets", 0);
	afficherTexte(ecran, 60, 150, 50, 20, "Monstres", 0);
	afficherTexte(ecran, 60, 200, 50, 20, "Fuir", 0);
	afficherTexte(ecran, 40, y, 50, 19, ">", 0);
	SDL_Flip(ecran);

	while(continuer)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:

				continuer = 0;
				break;
			case SDL_KEYDOWN:

				switch(event.key.keysym.sym)
				{
					case SDLK_UP:

						if(pos == 0)
						{
							pos = 3;
							y = 200;
						}
						else
						{
							pos--;
							y -= 50;
						}					
						break;
					case SDLK_DOWN:

						if(pos == 3)
						{
							pos = 0;
							y = 50;
						}
						else 
						{
							pos++;
							y+=50;
						}
						break;
					case SDLK_RETURN:

						switch(pos)
						{
							case 0:
								menuAttaquer(ecran, monMonstre, monstre);
								break;
							case 1:
								menuObjets(ecran, monDresseur, monMonstre);
								afficherStats(ecran, monMonstre, monstre);
								 break;
							case 2:
								monMonstre = menuMonstres(ecran, monDresseur, monMonstre, monstre);
								break;
							case 3:
								if(!dresseur)
								{
									continuer = 0;
									afficherTexte(ecran, 500, 540, 350, 60, "Vous prenez la fuite !", 1);
								}else afficherTexte(ecran, 500, 540, 400, 60, "Vous ne pouvez pas vous enfuir !", 1);
								a_joue = 0;
								break;
							default:
								break;
						}
						if(monstre->vie_actuelle == 0)
						{
							continuer = 0;
							nbMonstres--;
							printf("%d\n", nbMonstres);
							if(nbMonstres == 0)
							{
								afficherTexte(ecran, 500, 540, 350, 60, "gg, t'as win !", 1);
								argent = rand_a_b(0,50)+(monstre->niveau)*5;
								monDresseur->argent+=argent;
								sprintf(messageAttaque, "Vous avez obtenu %d coquillages !", argent);								afficherTexte(ecran, 500, 540, 400, 60, messageAttaque, 1);
								xp = 15 + (monstre->niveau)*10 /(3*(1+monMonstre->niveau));
								monMonstre->experience+=xp;
								
								memset(messageAttaque, 0, sizeof(messageAttaque));
								
								sprintf(messageAttaque, "%s a gagne %d points d'xp !", monMonstre->nom, xp);
								afficherTexte(ecran, 500, 540, 440, 60, messageAttaque, 1);
								
								memset(messageAttaque, 0, sizeof(messageAttaque));
								
								if(monMonstre->experience >= 100)
								{
									levelUp(monMonstre);
									sprintf(messageAttaque, "%s est passe au niveau %d !", monMonstre->nom, monMonstre->niveau);
									afficherTexte(ecran, 500, 540, 440, 60, messageAttaque, 1);
									monMonstre->experience-= 100;
									miseAJourDresseur(monDresseur);
									
									memset(messageAttaque, 0, sizeof(messageAttaque));
								}
								
								memset(messageAttaque, 0, sizeof(messageAttaque));
							}
						}
						if(continuer && a_joue)
						{
							choixattaque = choisirAttaque(monstre, monMonstre);
							if(rand_a_b(0,100) >= (100 - monstre->attaque[choixattaque]->precision))
							{
								monMonstre->vie_actuelle -= (monstre->attaque[choixattaque]->puissance)/4;
								monstre->vie_actuelle += (monstre->attaque[choixattaque]->heal)/2;
								sprintf(messageAttaque, "%s utilise %s !", monstre->nom, monstre->attaque[choixattaque]->nom);
								afficherTexte(ecran, 500, 540, 400, 60, messageAttaque, 1);
								if(multiplicateur(monstre->attaque[choixattaque]->type, monMonstre->type) > 1)
								{
									afficherTexte(ecran, 500, 540, 400, 60, "C'est super efficace !", 1);
								}
								if(multiplicateur(monstre->attaque[choixattaque]->type, monMonstre->type) < 1)
								{
									afficherTexte(ecran, 500, 540, 400, 60, "Ce n'est pas tres efficace...", 1);
								}
								afficherStats(ecran, monMonstre, monstre);
							}
							else afficherTexte(ecran, 500, 540, 400, 60, " Il  a rate ce con", 1);
								
							memset(messageAttaque, 0, sizeof(messageAttaque));
							if(monMonstre->vie_actuelle == 0)
							{
								sprintf(messageAttaque, "%s est K.O !", monMonstre->nom);
								afficherTexte(ecran, 500, 540, 400, 60, messageAttaque, 1);
								perdu = 1;
								for(i = 0 ; i < monDresseur->nb_monstres ; i++)
								{
									if(monDresseur->equipe[i]->vie_actuelle != 0)
									{
										perdu = 0;
									}
								}
								
								if(perdu)
								{
									afficherTexte(ecran, 500, 540, 400, 50, "Vous avez perdu. (loser)", 1);
									monMonstre = NULL;
									continuer = 0;
								
								}else monMonstre = menuMonstres(ecran, monDresseur, monMonstre, monstre);
								memset(messageAttaque, 0, sizeof(messageAttaque));
								
							}
							
						}
						break;

					default:
						break;
				}
			default:
				break;
		}
		
		SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));
		afficherTexte(ecran, 60, 50, 50, 20, "Attaquer", 0);
		afficherTexte(ecran, 60, 100, 50, 20, "Objets", 0);
		afficherTexte(ecran, 60, 150, 50, 20, "Monstres", 0);
		afficherTexte(ecran, 60, 200, 50, 20, "Fuir", 0);
		afficherTexte(ecran, 40, y, 50, 19, ">", 0);
		SDL_Flip(ecran);
	}

	return monMonstre;
}

void menuAttaquer(SDL_Surface* ecran, Monstre* monMonstre, Monstre* monstre)
{
	SDL_Rect rectangle;
	SDL_Event event;
	int continuer = 1;
	int pos = 0;
	int y = 50;
	char messageAttaque[30] = "";

	rectangle.x = 250;
	rectangle.y = 50;
	rectangle.w = 200;
	rectangle.h = 210;


	TTF_Font* font = NULL;

	font = TTF_OpenFont("Aller_Lt.ttf", 24);

	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));

	while(continuer)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				a_joue = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						a_joue = 0;
						continuer = 0;
						break;
					case SDLK_UP:
						if(pos == 0)
						{
							pos = 3;
							y = 200;
						}
						else
						{
							pos--;
							y -= 50;
						}					
						break;
					case SDLK_DOWN:
						if(pos == 3)
						{
							pos = 0;
							y = 50;
						}
						else 
						{
							pos++;
							y+=50;
						}
						break;
					case SDLK_RETURN:
						if(rand_a_b(0,100) >= (100 - monMonstre->attaque[pos]->precision))
						{
							sprintf(messageAttaque, "%s utilise %s !", monMonstre->nom, monMonstre->attaque[pos]->nom);
							afficherTexte(ecran, 500, 540, 400, 60, messageAttaque, 1);
							monstre->vie_actuelle -= multiplicateur(monMonstre->attaque[pos]->type, monstre->type)*(monMonstre->attaque[pos]->puissance)/4;
							monMonstre->vie_actuelle += (monMonstre->attaque[pos]->heal)/2;
							afficherStats(ecran, monMonstre, monstre);
							if(multiplicateur(monMonstre->attaque[pos]->type, monstre->type) > 1)
							{
								afficherTexte(ecran, 500, 540, 400, 60, "C'est super efficace !", 1);
							}
							if(multiplicateur(monMonstre->attaque[pos]->type, monstre->type) < 1)
							{
								afficherTexte(ecran, 500, 540, 400, 60, "Ce n'est pas tres efficace...", 1);
							}
						}
						else afficherTexte(ecran, 500, 540, 400, 60, "Attaque manquee, noob", 1);
						
						a_joue = 1;
						continuer = 0;
						break;
				}
		}
				SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));
				afficherTexte(ecran, 270, 50, 50, 20, monMonstre->attaque[0]->nom, 0);
				afficherTexte(ecran, 270, 100, 50, 20, monMonstre->attaque[1]->nom, 0);
				afficherTexte(ecran, 270, 150, 50, 20, monMonstre->attaque[2]->nom, 0);
				afficherTexte(ecran, 270, 200, 50, 20, monMonstre->attaque[3]->nom, 0);
				afficherTexte(ecran, 250, y, 50, 20, ">", 0);
				SDL_Flip(ecran);
	}
	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 0, 0, 0));
	TTF_CloseFont(font);
}

void menuObjets(SDL_Surface* ecran, Dresseur* monDresseur, Monstre* monMonstre)
{
	SDL_Rect rectangle;
	SDL_Event event;
	int continuer = 1;
	int pos = 0;
	int y = 50;
	int  i = 0;
	SDL_Surface* nb_objets;
	char m3[10] = "";

	rectangle.x = 250;
	rectangle.y = 50;
	rectangle.w = 300;
	rectangle.h = 210;


	TTF_Font* font = NULL;

	font = TTF_OpenFont("Aller_Lt.ttf", 24);

	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));

	while(continuer)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				a_joue = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						a_joue = 0;
						continuer = 0;
						break;
					case SDLK_UP:
						if(pos == 0)
						{
							pos = 3;
							y = 200;
						}
						else
						{
							pos--;
							y -= 50;
						}					
						break;
					case SDLK_DOWN:
						if(pos == 3)
						{
							pos = 0;
							y = 50;
						}
						else 
						{
							pos++;
							y+=50;
						}
						break;
					case SDLK_RETURN:
						switch(pos)
						{
							case POTION:
								if(monDresseur->Objets[POTION] > 0)
								{
									afficherTexte(ecran, 500, 540, 400, 60, "Vous utilisez une potion !", 1);
									monMonstre->vie_actuelle += 20;
									if(monMonstre->vie_actuelle > monMonstre->vie)
									{
										monMonstre->vie_actuelle = monMonstre->vie;
									}
									monDresseur->Objets[POTION]--;
									a_joue = 1;
									continuer = 0;
								}else afficherTexte(ecran, 500, 540, 400, 60, "Vous n'avez pas de potion !", 1);
								break;
							case SUPER:
								if(monDresseur->Objets[SUPER] > 0)
								{
									afficherTexte(ecran, 500, 540, 400, 60, "Vous utilisez une super potion !", 1);
									monMonstre->vie_actuelle += 50;
									if(monMonstre->vie_actuelle > monMonstre->vie)
									{
										monMonstre->vie_actuelle = monMonstre->vie;
									}
									monDresseur->Objets[SUPER]--;
									a_joue = 1;
									continuer = 0;
								}else afficherTexte(ecran, 500, 540, 400, 60, "Vous n'avez pas de super potion.", 1);
								break;
							case HYPER:
								if(monDresseur->Objets[HYPER] > 0)
								{
									afficherTexte(ecran, 500, 540, 400, 60, "Vous utilisez une hyper potion !", 1);
									monMonstre->vie_actuelle += 100;
									if(monMonstre->vie_actuelle > monMonstre->vie)
									{
										monMonstre->vie_actuelle = monMonstre->vie;
									}
									monDresseur->Objets[HYPER]--;
									a_joue = 1;
									continuer = 0;
								}else afficherTexte(ecran, 500, 540, 400, 60, "Vous n'avez pas d'hyper potion.", 1);
								break;
							case PAMPLEMOUSSE:
								if(monDresseur->Objets[PAMPLEMOUSSE] > 0)
								{
									afficherTexte(ecran, 500, 540, 400, 60, "Vous utilisez un pamplemousse !", 1);
									for( i = 0 ; i < monDresseur->nb_monstres ; i++)
									{
										monDresseur->equipe [i]-> vie_actuelle = monDresseur->equipe [i]->vie;
									}
									monDresseur->Objets[PAMPLEMOUSSE]--;
									a_joue = 1;
									continuer = 0;
								}
								else afficherTexte(ecran, 500, 540, 400, 60, "Vous n'avez pas de pamplemousse.", 1);
								break;
						}
						break;
				}
		}
			SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));
			for(i = 0 ; i < 4 ; i++)
			{
				sprintf(m3, "x %d ", monDresseur->Objets[i]);
				 nb_objets = TTF_RenderText_Solid(font, m3, textColor);
			 	apply_surface(460, 70+50*i, nb_objets, ecran);
			}
				afficherTexte(ecran, 270, 50, 50, 20, "Potion", 0);
				afficherTexte(ecran, 270, 100, 50, 20, "Super potion", 0);
				afficherTexte(ecran, 270, 150, 50, 20, "Hyper potion", 0);
				afficherTexte(ecran, 270, 200, 50, 20, "Pamplemousse", 0);
				afficherTexte(ecran, 250, y, 50, 20, ">", 0);
				SDL_Flip(ecran);
	}	

	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 0, 0, 0));
	SDL_FreeSurface(nb_objets);
	TTF_CloseFont(font);
}

Monstre* menuMonstres(SDL_Surface* ecran, Dresseur* monDresseur, Monstre* monMonstre, Monstre* monstre)
{
	SDL_Rect rectangle;
	SDL_Surface* hp;
	SDL_Event event;
	int continuer = 1;
	int pos = 0;
	int y = 50;
	int i;
	char message[50] = "";
	int nb = monDresseur->nb_monstres;

	rectangle.x = 250;
	rectangle.y = 50;
	rectangle.w = 300;
	rectangle.h = 210;

	TTF_Font* font = NULL;

	font = TTF_OpenFont("Aller_Lt.ttf", 18);
	
	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));
	
	char m3[30] = "";

	while(continuer)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				a_joue = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						continuer = 0;
						a_joue = 0;
						break;
					case SDLK_UP:
						if(pos == 0)
						{
							pos = nb-1;
							y = 50*nb;
						}
						else
						{
							pos--;
							y -= 50;
						}					
						break;
					case SDLK_DOWN:
						if(pos == nb-1)
						{
							pos = 0;
							y = 50;
						}
						else 
						{
							pos++;
							y+=50;
						}
						break;
					case SDLK_RETURN:
						if(monDresseur->equipe[pos] != monMonstre && monDresseur->equipe[pos]->vie_actuelle != 0)
						{
							monMonstre = monDresseur->equipe[pos];
							continuer = 0;
							a_joue = 1;
							strcat(message, monMonstre->nom);
							strcat(message, ", en avant !");
							afficherTexte(ecran, 500, 540, 400, 60, message, 1);
						}
						break;
				}
		}
			SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));
			for(i = 0 ; i < nb ; i++)
			{
				afficherTexte(ecran, 270, 50+50*i, 50, 20, monDresseur->equipe[i]->nom, 0);
				sprintf(m3, "%d / %d", monDresseur->equipe[i]->vie_actuelle, monDresseur->equipe[i]->vie);
				 hp = TTF_RenderText_Solid(font, m3, textColor);
			 	apply_surface(450, 70+50*i, hp, ecran);
			}
			afficherTexte(ecran, 250, y, 50, 20, ">", 0);
			SDL_Flip(ecran);
	}	
	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 0, 0, 0));
	afficherMonstre2(ecran, monMonstre);					
	afficherStats(ecran, monMonstre, monstre);
	SDL_FreeSurface(hp);
	TTF_CloseFont(font);
	return monMonstre;
}

Monstre* combatDresseur(SDL_Surface* ecran, Dresseur* monDresseur, Dresseur* dresseurAdverse)
{
	int j, i, k, l, a, ok = 0;
	Monstre* monstreAdverse = menuCombat(ecran, monDresseur, dresseurAdverse->equipe[0], dresseurAdverse->nb_monstres, 1);
	for( j = 1 ; j < dresseurAdverse->nb_monstres; j++)
	{
		switch(monstreAdverse->type)
		{
			case  EAU:
				for( k = 0 ; k < dresseurAdverse->nb_monstres ; k++)
				{
					if(dresseurAdverse->equipe[k]->type == PLANTE && dresseurAdverse->equipe[k]->vie_actuelle != 0)
					{
						menuCombat(ecran, monDresseur, dresseurAdverse->equipe[k], dresseurAdverse->nb_monstres - j, 1);
						ok = 1;
					}
				}
				break;
			case FEU:
				for( i = 0 ; i < dresseurAdverse->nb_monstres ; i++)
				{
					if(dresseurAdverse->equipe[i]->type == EAU && dresseurAdverse->equipe[i]->vie_actuelle != 0)
					{
						menuCombat(ecran, monDresseur, dresseurAdverse->equipe[i], dresseurAdverse->nb_monstres - j, 1);
						ok = 1;
					}
				}
				break;
			case PLANTE:
				for( l = 0 ; l < dresseurAdverse->nb_monstres ; l++)
				{
					if(dresseurAdverse->equipe[l]->type == FEU && dresseurAdverse->equipe[l]->vie_actuelle != 0)
					{
						menuCombat(ecran, monDresseur, dresseurAdverse->equipe[l], dresseurAdverse->nb_monstres - j, 1);
						ok = 1;
					}
				}
				break;
				
			default: 
				a = 0;
				while( !ok)
				{
					if(dresseurAdverse->equipe[a]->vie_actuelle > 0)
					{
						menuCombat(ecran, monDresseur, dresseurAdverse->equipe[a], dresseurAdverse->nb_monstres - j, 1);
						ok = 1;
					}
					a++;
				}	
				break;
		}
		if(!ok)
		{
			a = 0;
			while( !ok)
				{
					if(dresseurAdverse->equipe[a]->vie_actuelle > 0)
					{
						menuCombat(ecran, monDresseur, dresseurAdverse->equipe[a], dresseurAdverse->nb_monstres - j, 1);
						ok = 1;
					}
					a++;
				}
		}
		ok = 0;
	}
	if(perdu)
	{
		return NULL;
	}else return monstreAdverse;
}
			
		
		
		
	
