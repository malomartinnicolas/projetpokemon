#include "template_menu.h"

int main(){
	SDL_Surface *ecran=NULL;
	
	SDL_Init(SDL_INIT_VIDEO);
	SDL_WM_SetIcon(SDL_LoadBMP("./Images/hero_s_down.bmp"), NULL);
	ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 24, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0,0,0));
	SDL_WM_SetCaption("mon pkmn like", NULL);
	
	Monstre* mob = chargerMonstre("croco", EAU);
	
	sousmenu(ecran,mob->back,mob->front);
	
	SDL_Quit();
	return EXIT_SUCCESS;
} 
