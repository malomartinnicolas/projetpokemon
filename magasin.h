#ifndef DEF_MAGASIN
#define DEF_MAGASIN
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <SDL/SDL_ttf.h>
	#include "dresseur.h"
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "combat.h"
	
	void menuMagasin(SDL_Surface* ecran, Dresseur* monDresseur);
	
#endif