Jouer.exe: clean testmap.o map.o map_affichage.o map_deplacement.o monstres.o dresseur.o combat.o menu.o save.o magasin.o IA.o capture.o fenetre.o
		gcc testmap.o map.o map_affichage.o map_deplacement.o monstres.o dresseur.o combat.o menu.o save.o magasin.o IA.o capture.o fenetre.o -o Jouer.exe -lSDL -lSDL_image -lSDL_ttf  -Wall -Wno-switch 
		-rm *.o

monstres.o : monstres.c monstres.h
		gcc -c monstres.c -lSDL -lSDL_image -Wall -Wno-switch 

dresseur.o : dresseur.c dresseur.h
		gcc -c dresseur.c -lSDL -lSDL_image -Wall -Wno-switch 
	
testmap.o : testmap.c map_constantes.h
		gcc -c testmap.c -lSDL -lSDL_image -Wall -Wno-switch 
		
map.o : map.c map.h
		gcc -c map.c -lSDL -lSDL_image -Wall -Wno-switch 

map_affichage.o : map_affichage.c map_affichage.h
		gcc -c map_affichage.c -lSDL -lSDL_image -Wall -Wno-switch 

map_deplacement.o : map_deplacement.c map_deplacement.h
		gcc -c map_deplacement.c -lSDL -lSDL_image -Wall -Wno-switch 

combat.o : combat.c combat.h 
		gcc -c combat.c -lSDL -lSDL_image -lSDL_image -lSDL_ttf -Wall -Wno-switch 
		
menu.o : menu.c menu.h 
		gcc -c menu.c -lSDL -lSDL_image -lSDL_image -lSDL_ttf -Wall -Wno-switch 

save.o : save.c save.h
		gcc -c save.c -lSDL -lSDL_image -lSDL_image -lSDL_ttf -Wall -Wno-switch 
		
magasin.o : magasin.c magasin.h
		gcc -c magasin.c -lSDL -lSDL_image -lSDL_image -lSDL_ttf -Wall -Wno-switch 

IA.o : IA.c IA.h
		gcc -c IA.c -Wall -Wno-switch 
		
capture.o : capture.c capture.h
		gcc -c capture.c -lSDL -lSDL_image -lSDL_image -lSDL_ttf -Wall -Wno-switch 
		
fenetre.o : fenetre.c fenetre.h
		gcc -c fenetre.c -lSDL -lSDL_image -lSDL_image -lSDL_ttf -Wall -Wno-switch 

clean:
	-rm *.o -f
	-rm Jouer.exe -f
