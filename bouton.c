#include "bouton.h"

SDL_Surface* joliBouton(int actif, int hauteur, int largeur, char message[20], int police) {

	SDL_Rect positionBouton, positionTexte, positionBord;
	SDL_Surface *monBouton, *monTexte, *monBord;
	
	positionBord.w = largeur;
	positionBord.h = hauteur;
	positionBouton.w = largeur-5;
	positionBouton.h = hauteur-5;
	positionTexte.w = largeur-5;
	positionTexte.h = hauteur-5;	
	
	monBord = SDL_CreateRGBSurface(SDL_HWSURFACE, largeur, hauteur, 32, 0, 0, 0, 0);
	monBouton = SDL_CreateRGBSurface(SDL_HWSURFACE, largeur, hauteur, 32, 0, 0, 0, 0);
	
	SDL_FillRect(monBord, NULL, SDL_MapRGB(monBord->format, 0,0,0));
	
	if (!actif) {	
		SDL_FillRect(monBouton, NULL, SDL_MapRGB(monBouton->format, CLRPASSIF));
	} else {
		SDL_FillRect(monBouton, NULL, SDL_MapRGB(monBouton->format, CLRACTIF));
;	}
	
	TTF_Font* font = TTF_OpenFont("Aller_Lt.ttf", police);	
	monTexte = TTF_RenderText_Solid(font, message, textColor);
	
	SDL_BlitSurface(monBouton, NULL, monBord, &positionBouton);
	SDL_BlitSurface(monTexte, NULL, monBord, &positionTexte);	
	
	TTF_CloseFont(font);
	
	return monBord;	
	
}
