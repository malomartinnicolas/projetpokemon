#include "IA.h"

double multiplicateur(int a, int b)
{
	double res = 1.0;
	switch(a)
	{
		case PLANTE:
			res = (b==EAU)? 1+PRC : ((b==FEU) ? 1-PRC : 1.0);
			break; 
		
		case EAU:
			res = (b==FEU)? 1+PRC : ((b==PLANTE) ? 1-PRC : 1.0);
			break; 	
		
		case FEU:
			res = (b==PLANTE)? 1+PRC : ((b==EAU) ? 1-PRC : 1.0);
			break; 
		
		default:
			break;
	}
	return res;
}

int choisirAttaque(Monstre* att, Monstre* def)
{
	double eff[4];
	int res;
	int i;
	for(i=0;i<4;i++)
	{
		eff[i] = ((double) (att->force-def->force)/100+1)*(att->attaque[i]->puissance)*((double) (att->habilete-def->habilete)/100+1)*(att->attaque[i]->precision);
		eff[i] += (att->force+att->habilete)*(att->attaque[i]->heal/4+att->attaque[i]->precision);
	}

	int tirage = rand_a_b(0,100);
	if (tirage<10)
	{
		res=min(min(0,1),min(2,3));
	}
	else if (tirage<25)
	{
		res=max(min(0,1),min(2,3));
	}
	else if (tirage<50)
	{
		res=min(min(0,1),max(2,3));
	}
	else
	{
		res=max(max(0,1),max(2,3));
	}
	
	return res;
}
