#include "fenetre.h"





int CparLigne(int taille, int largeur)
{
	SDL_Color txtclr = {CLRTXT};
	TTF_Font* font = TTF_OpenFont(POLICE, taille);
	SDL_Surface* texte = NULL;
	char s[1000]="n";
	int nl=1;
	do
	{
	strcat(s,"n");
	texte = TTF_RenderText_Solid(font, s, txtclr);
	nl++;
	}while(texte->w<largeur);
	SDL_FreeSurface(texte);
	TTF_CloseFont(font);
	return nl-1;
}
	
void message(SDL_Surface* ecran, char* m, int h, int t)
{
	char* message = m;
	int taille = 1.2*t;
	int hauteur = (h<taille)?taille:h;
	hauteur = (hauteur/taille)*taille;
	SDL_Event event;
	SDL_Color txtclr = {CLRTXT};
	Uint32 fndclr=SDL_MapRGB(ecran->format,CLRFOND);
	TTF_Font* font = TTF_OpenFont(POLICE, taille);
	SDL_Surface *texte, *fond;
	fond = SDL_CreateRGBSurface(SDL_HWSURFACE, LARGEUR_FENETRE, hauteur, 32,0,0,0,0);
	SDL_FillRect(fond,NULL,fndclr);
	SDL_Rect pos;
	pos.x=0;
	pos.w=LARGEUR_FENETRE;
	
	
	
	int n = CparLigne(t, LARGEUR_FENETRE);
	int nt = strlen(message);
	char* ligne = malloc(n*sizeof(char));
	int i=0;
	while(i<nt)
	{
		strncpy(ligne,message,n);
		message+=n;
		i+=n;
		texte = TTF_RenderText_Solid(font, ligne, txtclr);
		pos.h = hauteur-taille;
		pos.y = taille;
		SDL_BlitSurface(fond,&pos,fond,NULL);
		pos.h = taille;
		pos.y = hauteur-taille*1.5;
		SDL_FillRect(fond,&pos,fndclr);
		SDL_BlitSurface(texte,NULL,fond,&pos);
		pos.h = hauteur;
		pos.y = HAUTEUR_FENETRE-hauteur;
		SDL_BlitSurface(fond,NULL,ecran,&pos);
		SDL_Flip(ecran);
		if (m[0]!=' ')
		{
			do
			{
				SDL_WaitEvent(&event);
			} while(event.type != SDL_KEYDOWN);
		}
	 }
	 SDL_FreeSurface(fond);
	 SDL_FreeSurface(texte);
	 free(ligne);
	 TTF_CloseFont(font);
}	


int menuMap(SDL_Surface* ecran,Dresseur* monDresseur)
{
	SDL_Event event;
	int continuer = 1;
	int i,now=0,last=0;
	SDL_Surface* options[NMENU];	
	SDL_Surface* ecrantmp = NULL;
	ecrantmp = SDL_CreateRGBSurface(SDL_HWSURFACE,ecran->w,ecran->h,32,0,0,0,0);
	SDL_BlitSurface(ecran, NULL, ecrantmp, NULL);
	char* messages[NMENU] = {"Monstres","Objets", "Dresseur","Sauvegarder", "Quitter"};
	SDL_Color txtclr = {CLRTXT};
	Uint32 actclr=SDL_MapRGB(ecran->format,CLRACTIF);
	Uint32 pasclr=SDL_MapRGB(ecran->format,CLRPASSIF);
	char tmp[40];
	
	for(i=0;i<NMENU;i++)
	{
		options[i] = joliBouton(150,messages[i],TAILLE,txtclr,pasclr);
	}
	
	
	while(continuer)
	{	
		SDL_BlitSurface(ecrantmp, NULL,ecran, NULL);
		
		SDL_FreeSurface(options[last]);
		options[last]=joliBouton(150,messages[last],TAILLE,txtclr,pasclr);
		SDL_FreeSurface(options[now]);
		options[now]=joliBouton(150,messages[now],TAILLE,txtclr,actclr);
		
		SDL_Rect rectangle;
		rectangle.x=0;
		rectangle.y=0;
	
		for(i=0;i<NMENU;i++)
		{
			SDL_BlitSurface(options[i],NULL,ecran, &rectangle);
			rectangle.y+=rectangle.h;
		}
	
		SDL_Flip(ecran);
			
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						last=now;
						now=(now+NMENU-1)%NMENU;
						break;
					case SDLK_DOWN:
						last=now;
						now=(now+NMENU+1)%NMENU;
						break;
					case SDLK_RETURN:
						switch(now)
						{
							case 0:
								if(monDresseur->nb_monstres>0)
								{
									menuMonstre(ecran,ecrantmp,monDresseur);
								}
								break;
							case 1:
								menuobjets(ecran,ecrantmp, monDresseur);
								break;
							case 2:
								rectangle.y=0;
								rectangle.x=0;
								sprintf(tmp,"nom:%s",monDresseur->nom);
								SDL_BlitSurface(joliBouton(250,tmp,TAILLE,txtclr,pasclr),NULL,ecran,&rectangle);
								rectangle.y+=TAILLE+10;
								sprintf(tmp,"argent:%d",monDresseur->argent);
								SDL_BlitSurface(joliBouton(250,tmp,TAILLE,txtclr,pasclr),NULL,ecran,&rectangle);
								rectangle.y+=TAILLE+10;
								sprintf(tmp,"niveau:%d",monDresseur->niveau);
								SDL_BlitSurface(joliBouton(250,tmp,TAILLE,txtclr,pasclr),NULL,ecran,&rectangle);
								sprintf(tmp,"sauvegarde:%d",monDresseur->save);
								rectangle.y+=TAILLE+10;
								SDL_BlitSurface(joliBouton(250,tmp,TAILLE,txtclr,pasclr),NULL,ecran,&rectangle);
								sprintf(tmp,"           ");
								rectangle.y+=TAILLE+10;
								SDL_BlitSurface(joliBouton(250,tmp,TAILLE,txtclr,pasclr),NULL,ecran,&rectangle);
								SDL_Flip(ecran);
								message(ecran,"appuyer sur une touche", 56,24);
								break;
							case 3:
								save(monDresseur);
								message(ecran,"Partie sauvegardee",56,24);
								break;
							case 4:
								continuer=0;
								return 0;
								break;
							default:
								break;
						}
						break;
					case SDLK_ESCAPE:
						continuer=0;
						break;
					default:
						break;
				}
			default:
				break;
		}
	}
	for(i=0;i<NMENU;i++)
	{
		SDL_FreeSurface(options[i]);
	}
	SDL_FreeSurface(ecrantmp);
	return 1;
}

void menuMonstre(SDL_Surface* ecran, SDL_Surface* ecrantmp,Dresseur* monDresseur)
{
	SDL_Event event;
	int continuer = 1;
	int i,now=0,last=0;
	SDL_Surface* options[NB_MONSTRES_MAX];
	message(ecran," R:relacher  U:leader",56,24);
	SDL_Color txtclr = {CLRTXT};
	Uint32 actclr=SDL_MapRGB(ecran->format,CLRACTIF);
	Uint32 pasclr=SDL_MapRGB(ecran->format,CLRPASSIF);
	
	char messages[NB_MONSTRES_MAX][100];
	for(i=0;i<NB_MONSTRES_MAX;i++)
	{
		sprintf(messages[i]," ");
		if (i<monDresseur->nb_monstres)
		{
			sprintf(messages[i],"%s %d %d/%d",monDresseur->equipe[i]->nom,monDresseur->equipe[i]->niveau,monDresseur->equipe[i]->vie_actuelle,monDresseur->equipe[i]->vie);
		}
		options[i] = joliBouton(300,messages[i],TAILLE,txtclr,pasclr);
	}
	
	
	while(continuer)
	{	
		
		SDL_BlitSurface(ecrantmp, NULL,ecran, NULL);
		
		SDL_FreeSurface(options[last]);
		
		
		options[last]=joliBouton(300,messages[last],TAILLE,txtclr,pasclr);
		SDL_FreeSurface(options[now]);
		options[now]=joliBouton(300,messages[now],TAILLE,txtclr,actclr);
		SDL_Rect rectangle;
		rectangle.x=0;
		rectangle.y=0;
	
		for(i=0;i<NB_MONSTRES_MAX;i++)
		{
			SDL_BlitSurface(options[i],NULL,ecran, &rectangle);
			rectangle.y+=rectangle.h;
		}
	
		SDL_Flip(ecran);
			
		SDL_WaitEvent(&event);
		
		
		
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						last=now;
						now=(now+monDresseur->nb_monstres-1)%monDresseur->nb_monstres;
						break;
					case SDLK_DOWN:
						last=now;
						now=(now+monDresseur->nb_monstres+1)%monDresseur->nb_monstres;
						break;
					case SDLK_r:
						relacher(monDresseur,now);
						for(i=0;i<NB_MONSTRES_MAX;i++)
						{
							sprintf(messages[i]," ");
							if (i<monDresseur->nb_monstres)
							{
								sprintf(messages[i],"%s %d %d/%d",monDresseur->equipe[i]->nom,monDresseur->equipe[i]->niveau,monDresseur->equipe[i]->vie_actuelle,monDresseur->equipe[i]->vie);
							}
							options[i] = joliBouton(300,messages[i],TAILLE,txtclr,pasclr);
						}
						break;
					case SDLK_u:
						putHead(monDresseur,now);
						for(i=0;i<NB_MONSTRES_MAX;i++)
						{
							sprintf(messages[i]," ");
							if (i<monDresseur->nb_monstres)
							{
								sprintf(messages[i],"%s %d %d/%d",monDresseur->equipe[i]->nom,monDresseur->equipe[i]->niveau,monDresseur->equipe[i]->vie_actuelle,monDresseur->equipe[i]->vie);
							}
							options[i] = joliBouton(300,messages[i],TAILLE,txtclr,pasclr);
						}
						break;
					case SDLK_ESCAPE:
						continuer=0;
						break;
					default:
						break;
					
				}
			default:
				break;
		}
		
	}
	for(i=0;i<NB_MONSTRES_MAX;i++)
	{
		SDL_FreeSurface(options[i]);
	}
}
	
void menuobjets(SDL_Surface* ecran, SDL_Surface* ecrantmp,Dresseur* monDresseur)
{
	SDL_Event event;
	int continuer = 1;
	int i,now=0,last=0;
	SDL_Surface* options[5];
	message(ecran," U:Utiliser",56,24);
	SDL_Color txtclr = {CLRTXT};
	Uint32 actclr=SDL_MapRGB(ecran->format,CLRACTIF);
	Uint32 pasclr=SDL_MapRGB(ecran->format,CLRPASSIF);
	
	char messages[5][20]={"Potion","Super Potion", "Hyper Potion","Pamplemousse","Retour"};
	
	for(i=0;i<4;i++)
	{
		sprintf(messages[i],"%s x%d",messages[i], monDresseur->Objets[i]);
		options[i] = joliBouton(200,messages[i],TAILLE,txtclr,pasclr);
	}
	options[4]=joliBouton(200,messages[PAMPLEMOUSSE+1], TAILLE,txtclr,pasclr);
	while(continuer)
	{	
		
		SDL_BlitSurface(ecrantmp, NULL,ecran, NULL);
		
		SDL_FreeSurface(options[last]);
		
		
		options[last]=joliBouton(200,messages[last],TAILLE,txtclr,pasclr);
		SDL_FreeSurface(options[now]);
		options[now]=joliBouton(200,messages[now],TAILLE,txtclr,actclr);
		SDL_Rect rectangle;
		rectangle.x=0;
		rectangle.y=0;
		for(i=0;i<5;i++)
		{
			SDL_BlitSurface(options[i],NULL,ecran, &rectangle);
			rectangle.y+=rectangle.h;
		}
	
		SDL_Flip(ecran);
			
		SDL_WaitEvent(&event);		
		
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						last=now;
						now=(now+5-1)%5;
						break;
					case SDLK_DOWN:
						last=now;
						now=(now+5+1)%5;
						break;
					case SDLK_RETURN:
						switch(now)
						{
							case (4):
								continuer=0;
								break;
							default:
								soinMonstre(ecran,ecrantmp,monDresseur, now);
								sprintf(messages[now],"%s x%d",messages[now], monDresseur->Objets[now]);
								break;
						}
						break;
					case SDLK_ESCAPE:
						continuer=0;
						break;
					default:
						break;
					
				}
			default:
				break;
		}
		
	}
	for(i=0;i<=PAMPLEMOUSSE+1;i++)
	{
		SDL_FreeSurface(options[i]);
	}
}
	
void soinMonstre(SDL_Surface* ecran, SDL_Surface* ecrantmp,Dresseur* monDresseur,int obj)
{
	SDL_Event event;
	int continuer = 1;
	int i,now=0,last=0;
	SDL_Surface* options[NB_MONSTRES_MAX];
	message(ecran," Selectionner le monstre à soigner puis 'entrer'",56,24);
	SDL_Color txtclr = {CLRTXT};
	Uint32 actclr=SDL_MapRGB(ecran->format,CLRACTIF);
	Uint32 pasclr=SDL_MapRGB(ecran->format,CLRPASSIF);
	
	char messages[NB_MONSTRES_MAX][100];
	for(i=0;i<NB_MONSTRES_MAX;i++)
	{
		sprintf(messages[i]," ");
		if (i<monDresseur->nb_monstres)
		{
			sprintf(messages[i],"%s %d %d/%d",monDresseur->equipe[i]->nom,monDresseur->equipe[i]->niveau,monDresseur->equipe[i]->vie_actuelle,monDresseur->equipe[i]->vie);
		}
		options[i] = joliBouton(300,messages[i],TAILLE,txtclr,pasclr);
	}
	
	
	while(continuer)
	{	
		
		SDL_BlitSurface(ecrantmp, NULL,ecran, NULL);
		
		SDL_FreeSurface(options[last]);
		
		
		options[last]=joliBouton(300,messages[last],TAILLE,txtclr,pasclr);
		SDL_FreeSurface(options[now]);
		options[now]=joliBouton(300,messages[now],TAILLE,txtclr,actclr);
		SDL_Rect rectangle;
		rectangle.x=0;
		rectangle.y=0;
	
		for(i=0;i<NB_MONSTRES_MAX;i++)
		{
			SDL_BlitSurface(options[i],NULL,ecran, &rectangle);
			rectangle.y+=rectangle.h;
		}
	
		SDL_Flip(ecran);
			
		SDL_WaitEvent(&event);
		
		
		
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						last=now;
						now=(now+monDresseur->nb_monstres-1)%monDresseur->nb_monstres;
						break;
					case SDLK_DOWN:
						last=now;
						now=(now+monDresseur->nb_monstres+1)%monDresseur->nb_monstres;
						break;
					case SDLK_RETURN:
						switch(obj)
						{
							case POTION:
								if(monDresseur->Objets[POTION] > 0)
								{
									message(ecran, "Vous utilisez une potion !",56,24);
									monDresseur->equipe[now]->vie_actuelle += 20;
								}else message(ecran, "Vous n'avez pas de potion !",56,24);
								break;
							case SUPER:
								if(monDresseur->Objets[SUPER] > 0)
								{
									message(ecran, "Vous utilisez une super potion !",56,24);
									monDresseur->equipe[now]->vie_actuelle += 50;
								}else message(ecran, "Vous n'avez pas de super potion.",56,24);
								break;
							case HYPER:
								if(monDresseur->Objets[HYPER] > 0)
								{
									message(ecran, "Vous utilisez une hyper potion !",56,24);
									monDresseur->equipe[now]->vie_actuelle += 100;
								}else message(ecran, "Vous n'avez pas d'hyper potion.",56,24);
								break;
							case PAMPLEMOUSSE:
								if(monDresseur->Objets[PAMPLEMOUSSE] > 0)
								{
									message(ecran, "Vous utilisez un pamplemousse !",56,24);
									for( i = 0 ; i < monDresseur->nb_monstres ; i++)
									{
										monDresseur->equipe [i]-> vie_actuelle = monDresseur->equipe [i]->vie;
									}
								}
								else message(ecran,"Vous n'avez pas de pamplemousse.", 56,24);
								break;
						}
						if(monDresseur->equipe[now]->vie_actuelle > monDresseur->equipe[now]->vie) monDresseur->equipe[now]->vie_actuelle = monDresseur->equipe[now]->vie;
						if (monDresseur->Objets[obj]>0) monDresseur->Objets[obj]--;
						sprintf(messages[now],"%s %d %d/%d",monDresseur->equipe[now]->nom,monDresseur->equipe[now]->niveau,monDresseur->equipe[now]->vie_actuelle,monDresseur->equipe[now]->vie);
						break;
					case SDLK_ESCAPE:
						continuer=0;
						break;
					default:
						break;
					
				}
			default:
				break;
		}
		
	}
	for(i=0;i<NB_MONSTRES_MAX;i++)
	{
		SDL_FreeSurface(options[i]);
	}
}	
	
	
	
