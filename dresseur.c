#include "dresseur.h"

Dresseur* creerDresseur(char nom[20], int nb_monstres, int niveau, Bestiaire* monBestiaire, Attaquaire* monAttaquaire)
{
	Dresseur* monDresseur = malloc(sizeof(Dresseur));
	strcpy(monDresseur->nom,nom);
	monDresseur->niveau=niveau;
	monDresseur->nb_monstres=nb_monstres;
	monDresseur->argent = 0;
	monDresseur->Objets[0] = 0;
	monDresseur->Objets[1] = 0;
	monDresseur->Objets[2] = 0;
	monDresseur->Objets[3] = 0;

	
	char path[50];
	sprintf(path,"./Images/Dresseurs/heros/s_down.bmp");
	monDresseur->front=SDL_LoadBMP(path);
	SDL_SetColorKey(monDresseur->front, SDL_SRCCOLORKEY, SDL_MapRGB(monDresseur->front->format, CLR));

	sprintf(path,"./Images/Dresseurs/heros/s_up.bmp");
	monDresseur->back=SDL_LoadBMP(path);
	SDL_SetColorKey(monDresseur->back, SDL_SRCCOLORKEY, SDL_MapRGB(monDresseur->back->format, CLR));

	sprintf(path,"./Images/Dresseurs/heros/s_right.bmp");
	monDresseur->right=SDL_LoadBMP(path);
	SDL_SetColorKey(monDresseur->right, SDL_SRCCOLORKEY, SDL_MapRGB(monDresseur->right->format, CLR));

	sprintf(path,"./Images/Dresseurs/heros/s_left.bmp");
	monDresseur->left=SDL_LoadBMP(path);
	SDL_SetColorKey(monDresseur->left, SDL_SRCCOLORKEY, SDL_MapRGB(monDresseur->left->format, CLR));
	
	int i;
	for(i = 0 ; i < nb_monstres ; i++) 		
	{
		monDresseur->equipe[i] = genererMonstre(EAU,niveau,monBestiaire,monAttaquaire);
	}
	

	return monDresseur;
}

void miseAJourDresseur(Dresseur* monDresseur)
{
	int i;
	int somme = 0;
	for(i = 0 ; i < monDresseur->nb_monstres ; i++) 		
	{
		somme += (monDresseur->equipe[i])->niveau;
	}
	monDresseur->niveau = somme/monDresseur->nb_monstres;
}

void libererDresseur(Dresseur* monDresseur)
{
	SDL_FreeSurface(monDresseur->front);
	SDL_FreeSurface(monDresseur->back);
	SDL_FreeSurface(monDresseur->right);
	SDL_FreeSurface(monDresseur->left);
	int i;
	for(i=0;i<monDresseur->nb_monstres;i++)
	{
		if (monDresseur->equipe[i] != NULL)
		{
			libererMonstre(monDresseur->equipe[i]);
		}
	}
	
	free(monDresseur);
}

int isAlive(Dresseur* monDresseur)
{
	int resultat=0;
	int i;
	for(i=0;i<monDresseur->nb_monstres;i++)
	{
		resultat+=((monDresseur->equipe[i]->vie_actuelle)!=0);
	}
	return resultat;
}

void putHead(Dresseur* monDresseur, int i)
{
		Monstre* tmp;
		tmp = monDresseur->equipe[0];
		monDresseur->equipe[0] = monDresseur->equipe[i];
		monDresseur->equipe[i] = tmp;
}

void relacher(Dresseur* monDresseur, int i)
{
	int j;
	for(j=i;j<monDresseur->nb_monstres-1;j++)
	{
		monDresseur->equipe[j]=monDresseur->equipe[j+1];
	}
	monDresseur->nb_monstres--;
}

void ajouter(Dresseur* monDresseur, Monstre* monMonstre)
{
	if(monDresseur->nb_monstres<NB_MONSTRES_MAX)
	{
		monDresseur->equipe[monDresseur->nb_monstres] = monMonstre;
		monDresseur->nb_monstres++;
	}
}




















	
