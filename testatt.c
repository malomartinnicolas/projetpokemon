#include "monstres.h"
#include "monstres.c"
#include <dirent.h>
#include <time.h>

void printAttaque(Attaque* monAttaque)
{
    printf("	%s: %d,%d,%d,%d,%d\n",monAttaque->nom,monAttaque->type,monAttaque->puissance,monAttaque->heal,monAttaque->precision,monAttaque->indice_rarete);

}

int rand_a_b(int a, int b)
{
	return rand()%(b-a)+a;
}

void main() 
{	
	srand(time(NULL));
	Attaquaire* monAttaquaire = chargerAttaquaire();
	Bestiaire* monBestiaire = chargerBestiaire();
	Monstre ** maListe = monBestiaire->monstres[EAU];
	int i,j;
	for(i=0;i<4;i++)
	{
		printf("Type %s : \n", TYPESTRING(i));
		for(j=0;j<NB_ATT_TYPE(i);j++)
		{
			printAttaque(*((monAttaquaire->attaques[i])+j));
		}
	}
	Monstre * monMonstre = copierMonstre(*(maListe +0));
	genererAttaqueAlea(monMonstre, monAttaquaire);
}
