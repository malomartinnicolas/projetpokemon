#include "map_constantes.h"
#include <SDL/SDL_ttf.h>
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

void afficherMenu(SDL_Surface *ecran);

int main()
{
	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();
	
	SDL_Surface *ecran = NULL;
	ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 24, SDL_HWSURFACE | SDL_DOUBLEBUF); //définition de la fenêtre
	afficherMenu(ecran);
	
	SDL_Quit();
	TTF_Quit();
	
	return EXIT_SUCCESS;
}
