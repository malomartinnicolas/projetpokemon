#include "capture.h"

Monstre* ecranCapture(SDL_Surface* ecran, Monstre* monstre, Dresseur* monDresseur)   //mob c'est pour afficher autour de menu, fait pas partie du template en soi
{
	SDL_Event event;
	int continuer = 1;
	int i,now=0,last=0;
	int tempsActuel=0;
	int tempsPrecedent=0;
	SDL_Surface* options[NBOPTIONS];
	
	char* messages[NBOPTIONS] ={"appater","caillasser", "filet", "fuir"};  //A faire pour toutes les options	
	SDL_Color txtclr = {CLRTXT};
	Uint32 actclr=SDL_MapRGB(ecran->format,CLRACTIF);
	Uint32 pasclr=SDL_MapRGB(ecran->format,CLRPASSIF);
	
	int estCapture = 0;
	int jauge = (rand_a_b(-10,11)*(100-monstre->indice_rarete)+1)/2;
	int nbCoups=0;
	for(i=0;i<NBOPTIONS;i++)
	{
		options[i] = joliBouton(120,messages[i],TAILLE,txtclr,pasclr);
	}
	
	SDL_Rect rectangle;
	
	SDL_Surface* dresseur = SDL_LoadBMP("./Images/Capture/herosback.bmp");
	SDL_SetColorKey(dresseur,SDL_SRCCOLORKEY, SDL_MapRGB(dresseur->format, CLR_TRSP));
	
	SDL_Surface* caillou = SDL_LoadBMP("./Images/Capture/caillou.bmp");
	SDL_SetColorKey(caillou,SDL_SRCCOLORKEY, SDL_MapRGB(caillou->format, CLR_TRSP));
	SDL_Surface* appat = SDL_LoadBMP("./Images/Capture/appat.bmp");
	SDL_SetColorKey(appat,SDL_SRCCOLORKEY, SDL_MapRGB(appat->format, CLR_TRSP));
	SDL_Surface* filet = SDL_LoadBMP("./Images/Capture/filet.bmp");
	SDL_SetColorKey(filet,SDL_SRCCOLORKEY, SDL_MapRGB(filet->format, CLR_TRSP));
	
	
	char* Cmonstre = malloc(sizeof(char)*(strlen(monstre->nom)+4));
	sprintf(Cmonstre,"%s lvl %d",monstre->nom,monstre->niveau);
	SDL_Surface* nom = joliBouton(strlen(Cmonstre)*TAILLE*0.63,Cmonstre,TAILLE,txtclr,actclr);
	free(Cmonstre);
	now=0;
	
	Prompteur* monPrompteur = creerPrompteur(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/3, txtclr,pasclr);
	
	while(continuer)
	{	
		SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0,0,0));
		SDL_FreeSurface(options[last]);
		options[last]=joliBouton(120,messages[last],TAILLE,txtclr,pasclr);
		SDL_FreeSurface(options[now]);
		options[now]=joliBouton(120,messages[now],TAILLE,txtclr,actclr);
		rectangle.x=0;
		rectangle.y=0;
	
		for(i=0;i<NBOPTIONS;i++)
		{
			SDL_BlitSurface(options[i],NULL,ecran, &rectangle);
			rectangle.y+=rectangle.h;
		}
		rectangle.x=LARGEUR_FENETRE-monPrompteur->lignes->w;
		rectangle.y=HAUTEUR_FENETRE-monPrompteur->lignes->h;
		SDL_BlitSurface(monPrompteur->lignes, NULL, ecran, &rectangle);
	
	
		rectangle.x=116;
		rectangle.y=542;
	
		switch(now)
		{
			case 0:
				//action1//
				SDL_BlitSurface(appat,NULL,ecran,&rectangle);
				break;
			case 1:
				//action2//
				SDL_BlitSurface(caillou,NULL,ecran,&rectangle);
				break;
			case 2:
				//action3//
				SDL_BlitSurface(filet,NULL,ecran,&rectangle);
				break;
			case 3:;
				break;
		}
	
		rectangle.y = 0;
		rectangle.x = LARGEUR_FENETRE-monstre->front->w;
		SDL_BlitSurface(monstre->front,NULL,ecran,&rectangle);
		rectangle.x-=nom->w;
		SDL_BlitSurface(nom,NULL,ecran,&rectangle);
	
		rectangle.y = HAUTEUR_FENETRE-dresseur->h ;
		rectangle.x=0;
		SDL_BlitSurface(dresseur,NULL,ecran,&rectangle);
		SDL_Flip(ecran);
			
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						last=now;
						now=(now+NBOPTIONS-1)%NBOPTIONS;
						break;
					case SDLK_DOWN:
						last=now;
						now=(now+NBOPTIONS+1)%NBOPTIONS;
						break;
					case SDLK_RETURN:
						rectangle.x=116;
						rectangle.y=542;
						SDL_Surface* tmp = SDL_CreateRGBSurface(SDL_HWSURFACE, LARGEUR_FENETRE, HAUTEUR_FENETRE, 32, 0, 0, 0, 0);
						SDL_BlitSurface(ecran, NULL,tmp,NULL);
						switch(now)
						{
							case 0:
								headshot(ecran, tmp, appat,rectangle);
								nbCoups++;
								ajouterMessage(monPrompteur, "Petit petit petit");
								if(jauge<0)
								{
									ajouterMessage(monPrompteur, "Le monstre se calme en mangeant");
								}
								else
								{
									ajouterMessage(monPrompteur, "Le monstre continue de manger");
								}
								jauge+=(double)(rand_a_b(15,200-monDresseur->niveau)*monstre->indice_rarete/50);
								if ( (jauge<3*monstre->indice_rarete)&&(jauge>-3*monstre->indice_rarete) )
								{
								ajouterMessage(monPrompteur,"Le monstre ronronne...");
								}
								break;
							case 1:
								headshot(ecran, tmp, caillou,rectangle);
								nbCoups++;
								ajouterMessage(monPrompteur, "Tiens, prends ca !");
								if(jauge<0)
								{
									ajouterMessage(monPrompteur, "Le monstre enrage");
								}
								else
								{
									ajouterMessage(monPrompteur, "Le monstre s'affaiblit");
								}
								jauge-=(double)(rand_a_b(15,200-monDresseur->niveau)*monstre->indice_rarete/50);
								if ( (jauge<3*monstre->indice_rarete)&&(jauge>-3*monstre->indice_rarete) )
								{
								ajouterMessage(monPrompteur,"Le monstre ronronne...");
								}
								break;
							case 2:
								headshot(ecran, tmp, filet,rectangle);
								nbCoups++;
								ajouterMessage(monPrompteur, "Et hop !");
								if ( (jauge<2*monstre->indice_rarete)&&(jauge>-2*monstre->indice_rarete) )
								{
									ajouterMessage(monPrompteur, "je t'ai eu !");
									continuer=0;
									estCapture = 1;
								}
								else
								{
								jauge*=2;
								ajouterMessage(monPrompteur, "mince, rate");
								if ( (jauge<3*monstre->indice_rarete)&&(jauge>-3*monstre->indice_rarete) )
								{
								ajouterMessage(monPrompteur,"Le monstre ronronne...");
								}
								}
								break;
							case 3:
								//action4//
								ajouterMessage(monPrompteur, "il sert a rien lui, ciao !");
								continuer=0;
								break;
						}
						SDL_FreeSurface(tmp);
					default:
						break;
				}
			break;
		}
		tempsActuel = SDL_GetTicks();
		if(tempsActuel-tempsPrecedent >50)
		{	
			if(nbCoups>12)
			{
			ajouterMessage(monPrompteur, "le monstre en a ras le cul...");
			ajouterMessage(monPrompteur, "...il s'enfuit");
			continuer=0;
			}
			if(jauge>100*monstre->indice_rarete)
			{
				ajouterMessage(monPrompteur, "le monstre rassasié s'enfuit");
				continuer=0;
			}
			if(jauge<-100*monstre->indice_rarete)
			{
				ajouterMessage(monPrompteur, "le monstre appeuré s'enfuit");
				continuer=0;
			}
			rectangle.x=LARGEUR_FENETRE-monPrompteur->lignes->w;
			rectangle.y=HAUTEUR_FENETRE-monPrompteur->lignes->h;
			SDL_BlitSurface(monPrompteur->lignes, NULL, ecran, &rectangle);
			SDL_Flip(ecran);
			
			tempsPrecedent=tempsActuel;
		}
		else
		{
			SDL_Delay(50 - (tempsActuel - tempsPrecedent));
		}
	}
	
	SDL_WaitEvent(&event);
	do
	{
	SDL_WaitEvent(&event);
	} while(event.type != SDL_KEYDOWN);
	
	for(i=0;i<NBOPTIONS;i++)
	{
		SDL_FreeSurface(options[i]);
	}
	SDL_FreeSurface(nom);
	SDL_FreeSurface(dresseur);
	SDL_FreeSurface(caillou);
	SDL_FreeSurface(appat);
	SDL_FreeSurface(filet);
	libererPrompteur(monPrompteur);
	if (estCapture)
	{
		return monstre;
	}
	else
	{
		return NULL;
	}
}


SDL_Surface* joliBouton(int largeur, char message[100], int taillePolice, SDL_Color txtclr, Uint32 fndclr)
{
	SDL_Rect positionBouton, positionTexte;
	SDL_Surface *monBouton, *monTexte, *monBord;	
	
	monBord = SDL_CreateRGBSurface(SDL_HWSURFACE, largeur, taillePolice+10, 32, 0, 0, 0, 0);
	SDL_FillRect(monBord, NULL, SDL_MapRGB(monBord->format, 0,0,0));
	
	monBouton = SDL_CreateRGBSurface(SDL_HWSURFACE, largeur, taillePolice+8, 32, 0, 0, 0, 0);
	positionBouton.x = 2;
	positionBouton.y = 2;	
	SDL_FillRect(monBouton, NULL, fndclr);
	SDL_BlitSurface(monBouton, NULL, monBord, &positionBouton);
	
	TTF_Font* font = NULL;
	font = TTF_OpenFont(POLICE, taillePolice);	
	monTexte = TTF_RenderText_Solid(font, message, txtclr);
	positionTexte.x = 4;
	positionTexte.y = 4;
	SDL_BlitSurface(monTexte, NULL, monBord, &positionTexte);	
	
	SDL_FreeSurface(monBouton);
	SDL_FreeSurface(monTexte);
	TTF_CloseFont(font);
	
	return monBord;
}

	
Prompteur* creerPrompteur(int w,int h, SDL_Color texteCouleur, Uint32 fondCouleur)
{
	Prompteur* monPrompteur = malloc(sizeof(Prompteur));
	monPrompteur->lignes = SDL_CreateRGBSurface(SDL_HWSURFACE, w, h, 32,0,0,0,0);
	monPrompteur->texteCouleur = texteCouleur;
	monPrompteur->fondCouleur = fondCouleur;
	SDL_FillRect(monPrompteur->lignes, NULL, monPrompteur->fondCouleur);
	monPrompteur->taille = h/NBMESSAGES;
	monPrompteur->police = TTF_OpenFont(POLICE, monPrompteur->taille);
	int i;
	for(i=0;i<NBMESSAGES;i++)
	{
	monPrompteur->messages[i]= TTF_RenderText_Solid(monPrompteur->police, "", monPrompteur->texteCouleur);
	}
	return monPrompteur;
}

void libererPrompteur(Prompteur* monPrompteur)
{
	int i;
	for(i=0;i<NBMESSAGES;i++)
	{
		SDL_FreeSurface(monPrompteur->messages[i]);
	}
	SDL_FreeSurface(monPrompteur->lignes);
}

void ajouterMessage(Prompteur* monPrompteur, char message[100])
{
	SDL_FillRect(monPrompteur->lignes, NULL,  monPrompteur->fondCouleur);
	int i;
	SDL_Rect pos;
	pos.x=0;
	pos.y=0;
	//SDL_FreeSurface(monPrompteur->messages[NBMESSAGES]);
	for(i=NBMESSAGES-1;i>0;i--)
	{
		monPrompteur->messages[i]=monPrompteur->messages[i-1];
		SDL_BlitSurface(monPrompteur->messages[i],NULL,monPrompteur->lignes,&pos);
		pos.y+=monPrompteur->taille;
	}
	monPrompteur->messages[0] = TTF_RenderText_Solid(monPrompteur->police, message, monPrompteur->texteCouleur);
	SDL_BlitSurface(monPrompteur->messages[0],NULL,monPrompteur->lignes,&pos);
}

void headshot(SDL_Surface* ecran, SDL_Surface* fond, SDL_Surface* obj, SDL_Rect ori)
{ 
	int x=ori.x;
	for(x=ori.x;x<820;x+=8)
	{
		ori.x=x;
		ori.y-=5;
		SDL_BlitSurface(fond,NULL,ecran,NULL);
		SDL_BlitSurface(obj,NULL,ecran,&ori);
		SDL_Flip(ecran);
	}
	SDL_BlitSurface(fond,NULL,ecran,NULL);
	SDL_Flip(ecran);
}
 
 
 
 
 
