#include "magasin.h"

void menuMagasin(SDL_Surface* ecran, Dresseur* monDresseur)
{
	SDL_Rect rectangle;
	SDL_Event event;
	int continuer = 1;
	int pos = 0;
	int y = 320;
	int prix[4] = {200, 700, 2000, 10000};
	
	rectangle.x = 150;
	rectangle.y = 320;
	rectangle.w = 340;
	rectangle.h = 210;

	TTF_Font* font = NULL;

	font = TTF_OpenFont("Aller_Lt.ttf", 24);

	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));

	while(continuer)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						continuer = 0;
						break;
					case SDLK_UP:
						if(pos == 0)
						{
							pos = 3;
							y = 470;
						}
						else
						{
							pos--;
							y -= 50;
						}					
						break;
					case SDLK_DOWN:
						if(pos == 3)
						{
							pos = 0;
							y = 320;
						}
						else 
						{
							pos++;
							y+=50;
						}
						break;
					case SDLK_RETURN:
						if(monDresseur->argent < prix[pos])
						{
							afficherTexte(ecran, 500, 395, 455, 60, "Vous n'avez pas suffisamment d'argent.", 1);
							SDL_Flip(ecran);
						}
						else
						{
							monDresseur->Objets[pos]+=1;
							monDresseur->argent -= prix[pos];
							switch(pos)
							{
							 	case 0:
							 		afficherTexte(ecran, 500, 395, 455, 60, "Vous avez acquis une potion.", 1);
							 		break;
							 	case 1:
							 		afficherTexte(ecran, 500, 395, 455, 60, "Vous avez acquis une super potion.", 1);
							 		break;
							 	case 2:
							 		afficherTexte(ecran, 500, 395, 455, 60, "Vous avez acquis une hyper potion.", 1);
							 		break;
							 	case 3:
							 		afficherTexte(ecran, 500, 395, 455, 60, "Vous avez acquis un pamplemousse.", 1);
							 		break;
							 	default:
							 		break;
							 }
							 SDL_Flip(ecran);
						}
						break;
				}
		}
		switch(pos)
		{
		 	case 0:
		 		afficherTexte(ecran, 500, 395, 455, 60, "Rend 20 points de vie a votre monstre.", 0);
		 		break;
		 	case 1:
		 		afficherTexte(ecran, 500, 395, 455, 60, "Rend 50 points de vie a votre monstre.", 0);
		 		break;
		 	case 2:
		 		afficherTexte(ecran, 500, 395, 455, 60, "Rend 100 points de vie a votre monstre.", 0);
		 		break;
		 	case 3:
		 		afficherTexte(ecran, 500, 395, 455, 60, "Soigne tous les monstres.", 0);
		 		break;
		 	default:
		 		break;
		 }
		SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 255, 255, 255));
		afficherTexte(ecran, 200, 25, 550, 60, "Bienvenue chez le marchand, potions en vente !", 0);
		afficherTexte(ecran, 170, 320, 50, 20, "Potion									 200 $", 0);
		afficherTexte(ecran, 170, 370, 50, 20, "Super potion			  700 $", 0);
		afficherTexte(ecran, 170, 420, 50, 20, "Hyper potion			  2000 $", 0);
		afficherTexte(ecran, 170, 470, 50, 20, "Pamplemousse		10000 $", 0);
		afficherTexte(ecran, 150, y, 50, 20, ">", 0);
		SDL_Flip(ecran);
	}	

	SDL_FillRect(ecran, &rectangle, SDL_MapRGB(ecran->format, 0, 0, 0));
	TTF_CloseFont(font);
}
