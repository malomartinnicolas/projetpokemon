




void menumap(SDL_Surface* ecran)
{

	SDL_Event event;
	int continuer = 1;
	int i,now=0,last=0;
	int tempsActuel=0;
	int tempsPrecedent=0;
	SDL_Surface* options[NMENU];	
	
	char* messages[NMENU] =NULL;
	messages = {"Monstres","Objets", "Sauvegarder", "Retour"};
	SDL_Color txtclr = {CLRTXT};
	Uint32 actclr=SDL_MapRGB(ecran->format,CLRACTIF);
	Uint32 pasclr=SDL_MapRGB(ecran->format,CLRPASSIF);
	
	for(i=0;i<NMENU;i++)
	{
		options[i] = joliBouton(120,messages[i],TAILLE,txtclr,pasclr);
	}
	
	
	while(continuer)
	{	
		SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0,0,0));
		SDL_FreeSurface(options[last]);
		options[last]=joliBouton(120,messages[last],TAILLE,txtclr,pasclr);
		SDL_FreeSurface(options[now]);
		options[now]=joliBouton(120,messages[now],TAILLE,txtclr,actclr);
		
		SDL_Rect rectangle;
		rectangle.x=0;
		rectangle.y=0;
	
		for(i=0;i<NMENU;i++)
		{
			SDL_BlitSurface(options[i],NULL,ecran, &rectangle);
			rectangle.y+=rectangle.h;
		}
	
		SDL_Flip(ecran);
			
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						last=now;
						now=(now+NMENU-1)%NMENU;
						break;
					case SDLK_DOWN:
						last=now;
						now=(now+NMENU+1)%NMENU;
						break;
					case SDLK_RETURN:
						switch(now)
						{
							case 0:
								//menuMonstre();
								break;
							case 1:
								//menuObjet();
								break;
							case 2:
								save(monDresseur, getSave());
								message(ecran,"Partie sauvegardee",56,24);
								break;
							case 3:
								continuer = 0;
								break;
							default:
								break;
						}
					case SDLK_ESCAPE:
						continuer=0;
						break;
					default:
						break;
				}
			default:
				break;
		}
		tempsActuel = SDL_GetTicks();
		if(tempsActuel-tempsPrecedent >50)
			tempsPrecedent=tempsActuel;
		else
			SDL_Delay(50 - (tempsActuel - tempsPrecedent));
	}
	for(i=0;i<NMENU;i++)
	{
		SDL_FreeSurface(options[i]);
	}
}
