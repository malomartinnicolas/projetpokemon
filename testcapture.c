#include "capture.h"
#include "map_affichage.c"
#include "map_deplacement.c"
#include "map.c"
#include "capture.c"
#include "dresseur.c"
#include "monstres.c"
#include "combat.c"
#include "save.c"
#include "menu.c"
#include "IA.c"
#include "magasin.c"



int main(){
	TTF_Init();
	SDL_Surface *ecran=NULL;
	srand(time(NULL));
	SDL_Init(SDL_INIT_VIDEO);
	SDL_WM_SetIcon(SDL_LoadBMP("./Images/hero_s_down.bmp"), NULL);
	ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 24, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0,0,0));
	SDL_WM_SetCaption("mon pkmn like", NULL);
	Monstre* mob = chargerMonstre("croco", EAU);
	SDL_Surface* dresseurback = SDL_LoadBMP("./Images/Dresseurs/heros/s_up.bmp");
	SDL_SetColorKey(dresseurback,SDL_SRCCOLORKEY, SDL_MapRGB(dresseurback->format, CLR_TRSP));
	
	Parametres* mesParametres = chargerParametres(0,0,0,CENTRE);
	Dresseur* monDresseur = creerDresseur("Sacha",4,1,mesParametres->monBestiaire, mesParametres->monAttaquaire);
	ecranCapture(ecran,mob,monDresseur);
	
	SDL_Quit();
	return EXIT_SUCCESS;
}
