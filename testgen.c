#include "monstres.h"
#include "monstres.c"
#include <time.h>

void printMonstre(Monstre* monMonstre)
{
	int i;	
	printf("%s: %d,%d,%d,%d\n",monMonstre->nom,monMonstre->vie,monMonstre->force,monMonstre->habilete,monMonstre->indice_rarete);
}



int main(){
	srand(time(NULL));
	int i;
	Bestiaire* monBestiaire= chargerBestiaire();
	for (i=0;i<41;i++)
	{
	Monstre* monMonstre = genererMonstre(NORMAL,monBestiaire);
	printMonstre(monMonstre);
	}
	return 1;
}
