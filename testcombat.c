#include "combat.h"
#include "combat.c"
#include "monstres.h"
#include "dresseur.h"
#include "monstres.c"
#include "dresseur.c"
#include "map_constantes.h"
#include <time.h>

int rand_a_b(int a, int b)
{
	return rand()%(b-a)+a;
}

int main()
{
	srand(time(NULL));

	SDL_Surface *ecran = NULL;
	SDL_Event event;
	int continuer=1;

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();
	ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 24, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_WM_SetCaption("mon pkmn like", NULL);

	Attaquaire* monAttaquaire = chargerAttaquaire();
	Bestiaire* monBestiaire = chargerBestiaire();

	//Monstre* monstre = genererMonstre (NORMAL, monBestiaire, 10, monAttaquaire);
	//Monstre* monstre = chargerMonstre("ogre", NORMAL);

	Dresseur* monDresseur = creerDresseur("Chocolat", 4, 5, monBestiaire, monAttaquaire);
	Dresseur* dresseur = creerDresseur("Sacha", 2, 5, monBestiaire, monAttaquaire);

	while(continuer)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						continuer = 0;
						break;
					case SDLK_SPACE:
						//menuCombat(ecran, monDresseur, dresseur->equipe[0], monstre, 1, 0);
						combatDresseur(ecran, monDresseur, dresseur);
						break;
				}
		}
		SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
		SDL_Flip(ecran);
	}
	TTF_Quit();
	SDL_Quit();

	return EXIT_SUCCESS;
}