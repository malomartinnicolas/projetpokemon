#ifndef DEF_DSR
#define DEF_DSR

	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>
	#include "monstres.h"
	
	

	#define NB_MONSTRES_MAX 4
	
	enum {HEROS, PASHEROS};
	enum{POTION, SUPER, HYPER, PAMPLEMOUSSE};

	typedef struct
	{
	char nom[20];
	int nb_monstres, niveau, argent;
	SDL_Surface *front, *back, *right, *left;
	int Objets[4];
	int save;
	Monstre* equipe[NB_MONSTRES_MAX];
	} Dresseur; 
	
	
	void miseAJourDresseur(Dresseur* monDresseur);
	Dresseur* creerDresseur(char nom[20], int nb_monstres, int niveau, Bestiaire* monBestiaire, Attaquaire* monAttaquaire);
	void libererDresseur(Dresseur* monDresseur);
	int isAlive(Dresseur*);
	void putHead(Dresseur* monDresseur, int i);
	void relacher(Dresseur* monDresseur, int i);
	void ajouter(Dresseur* monDresseur, Monstre* monMonstre);
#endif
