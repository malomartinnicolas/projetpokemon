#ifndef DEF_CAPTURE
#define DEF_CAPTURE	
	#include <stdlib.h>
	#include <stdio.h>
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <SDL/SDL_ttf.h>
	#include "map_constantes.h"
	#include "rand.h"		     
	
	#define NBOPTIONS 4
	#define NBMESSAGES 9
	
	SDL_Surface* joliBouton(int largeur, char message[20], int taillePolice, SDL_Color txtclr, Uint32 fndclr);
	Monstre* ecranCapture(SDL_Surface* ecran, Monstre* monstre, Dresseur* monDresseur);  
	typedef struct
	{
	SDL_Surface* lignes;
	int taille;
	TTF_Font* police;
	SDL_Surface* messages[NBMESSAGES];
	SDL_Color texteCouleur;
	Uint32 fondCouleur;
	} Prompteur;

	Prompteur* creerPrompteur(int w,int h, SDL_Color texteCouleur, Uint32 fondCouleur);
	void libererPrompteur(Prompteur* monPrompteur);
	void ajouterMessage(Prompteur* monPrompteur, char message[100]);
	void headshot(SDL_Surface* ecran, SDL_Surface* fond, SDL_Surface* obj, SDL_Rect ori);
#endif
