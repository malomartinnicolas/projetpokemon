#ifndef IA
#define IA

	#include "monstres.h"
	#define max(a,b) ((eff[a])>(eff[b])?(a):(b))
	#define min(a,b) ((eff[a])<(eff[b])?(a):(b))
	#define PRC 0.2

	int choisirAttaque(Monstre* att, Monstre* def);
	double multiplicateur(int a, int b);
	
#endif