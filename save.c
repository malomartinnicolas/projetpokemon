#include "save.h"

void save(Dresseur* monDresseur)
{
	int i, j;
	char path[100];

	sprintf(path,"./SAVE/save%d.txt",monDresseur->save);
	FILE* savefile = fopen(path, "w");

	fprintf(savefile,"nom:%s\nniveau:%d\nnb_monstres:%d\nargent:%d\n",monDresseur->nom, monDresseur->niveau, monDresseur->nb_monstres, monDresseur->argent);		//carac dresseur
	
	for( i = 0 ; i < 4 ; i++)
	{
		fprintf(savefile,"obj:%d\n", monDresseur->Objets[i]);
	}
	
	for( i = 0 ; i < monDresseur->nb_monstres ; i++)
	{
		fprintf(savefile,"monstre%d:%s\ntype:%d\nniveau:%d\nvie:%d\nforce:%d\nhabilete:%d\nindice:%d\nvie_actuelle:%d\nxp:%d\n", i, monDresseur->equipe[i]->nom, monDresseur->equipe[i]->type, monDresseur->equipe[i]->niveau, monDresseur->equipe[i]->vie, monDresseur->equipe[i]->force, monDresseur->equipe[i]->habilete, monDresseur->equipe[i]->indice_rarete, monDresseur->equipe[i]->vie_actuelle, monDresseur->equipe[i]->experience);
		
		for( j = 0 ; j < 4 ; j++)
		{
			fprintf(savefile, "attaque%d:%s\ntype:%d\npuissance:%d\nheal:%d\nprecision:%d\nindice:%d\n", j, monDresseur->equipe[i]->attaque[j]->nom, monDresseur->equipe[i]->attaque[j]->type, monDresseur->equipe[i]->attaque[j]->puissance, monDresseur->equipe[i]->attaque[j]->heal, monDresseur->equipe[i]->attaque[j]->precision, monDresseur->equipe[i]->attaque[j]->indice_rarete);
		}
	}
	fclose(savefile);
}

Dresseur* load(int n)
{
	int i, j;
	Dresseur* monDresseur = malloc(sizeof(Dresseur));
	monDresseur->save=n;
	char path[100];
	
	sprintf(path,"./SAVE/save%d.txt",n);
	FILE* savefile = fopen(path, "r");

	fscanf(savefile,"nom:%s\nniveau:%d\nnb_monstres:%d\nargent:%d\n",monDresseur->nom, &(monDresseur->niveau), &(monDresseur->nb_monstres), &(monDresseur->argent));		//carac dresseur
	
	for( i = 0 ; i < 4 ; i++)
	{
		fscanf(savefile,"obj:%d\n", &(monDresseur->Objets[i]));
	}
	
	for( i = 0 ; i < monDresseur->nb_monstres ; i++)
	{
		(*monDresseur).equipe[i] =  malloc(sizeof(Monstre));

		fscanf(savefile,"monstre%d:%s\ntype:%d\nniveau:%d\nvie:%d\nforce:%d\nhabilete:%d\nindice:%d\nvie_actuelle:%d\nxp:%d\n", &i, monDresseur->equipe[i]->nom, &(monDresseur->equipe[i]->type), &(monDresseur->equipe[i]->niveau), &(monDresseur->equipe[i]->vie), &(monDresseur->equipe[i]->force), &(monDresseur->equipe[i]->habilete), &(monDresseur->equipe[i]->indice_rarete), &(monDresseur->equipe[i]->vie_actuelle), &(monDresseur->equipe[i]->experience));

		for( j = 0 ; j < 4 ; j++)
		{
			(*monDresseur->equipe[i]).attaque[j] =  malloc(sizeof(Attaque));

			fscanf(savefile, "attaque%d:%s\ntype:%d\npuissance:%d\nheal:%d\nprecision:%d\nindice:%d\n", &j, monDresseur->equipe[i]->attaque[j]->nom, &monDresseur->equipe[i]->attaque[j]->type, &monDresseur->equipe[i]->attaque[j]->puissance, &monDresseur->equipe[i]->attaque[j]->heal, &monDresseur->equipe[i]->attaque[j]->precision, &monDresseur->equipe[i]->attaque[j]->indice_rarete);
		}
		chargerImagesMonstre(monDresseur->equipe[i]);

	}
	return monDresseur;
}

