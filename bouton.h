#ifndef MENU
#define MENU

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#define CLRACTIF 51,0,51
#define CLRPASSIF 255,51,0

const SDL_Color textColor = {0, 0, 0};

SDL_Surface* joliBouton(int actif, int hauteur, int largeur, char message[20], int police);

#endif
