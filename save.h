#ifndef SAVE
#define SAVE
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "monstres.h"
	#include "dresseur.h"
	
	void save(Dresseur* monDresseur);
	Dresseur* load(int n);
	
#endif
