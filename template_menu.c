#include "template_menu.h"

void sousmenu(SDL_Surface* ecran, SDL_Surface* BG, SDL_Surface* HD)   //mob c'est pour afficher autour de menu, fait pas partie du template en soi
{
	TTF_Init();
	SDL_Event event;
	int continuer = 1;
	int i,now=0,last=0;
	SDL_Surface* options[NBOPTIONS];
	
	char* messages[NBOPTIONS] ={"apater","caillasser", "filet", "fuir"};  //A faire pour toutes les options	
	SDL_Color txtclr = {CLRTXT};
	Uint32 actclr=SDL_MapRGB(ecran->format,CLRACTIF);
	Uint32 pasclr=SDL_MapRGB(ecran->format,CLRPASSIF);
	
	
	
	for(i=0;i<NBOPTIONS;i++)
	{
		options[i] = joliBouton(120,messages[i],TAILLE,txtclr,pasclr);
	}
	
	SDL_Rect rectangle;

	rectangle.y = 0;
	rectangle.x = LARGEUR_FENETRE-HD->w;
	SDL_BlitSurface(HD,NULL,ecran,&rectangle);
	
	rectangle.y = HAUTEUR_FENETRE-BG->h ;
	rectangle.x=0;
	SDL_BlitSurface(BG,NULL,ecran,&rectangle);
	
	now=0;
	
	Prompteur* monPrompteur = creerPrompteur(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/3, txtclr,pasclr);
	
	while(continuer)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer = 0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_UP:
						last=now;
						now=(now+NBOPTIONS-1)%NBOPTIONS;
						break;
					case SDLK_DOWN:
						last=now;
						now=(now+NBOPTIONS+1)%NBOPTIONS;
						break;
					case SDLK_RETURN:
						switch(now)
						{
							case 0:
								//action1//
								ajouterMessage(monPrompteur, messages[now]);
								break;
							case 1:
								//action2//
								ajouterMessage(monPrompteur, messages[now]);
								break;
							case 2:
								//action3//
								ajouterMessage(monPrompteur, messages[now]);
								break;
							case 3:
								//action4//
								ajouterMessage(monPrompteur, messages[now]);
								break;
						}
					default:
						break;
				}
			break;
		}
		
		options[last]=joliBouton(120,messages[last],TAILLE,txtclr,pasclr);
		options[now]=joliBouton(120,messages[now],TAILLE,txtclr,actclr);
		rectangle.x=0;
		rectangle.y=0;
		
		for(i=0;i<NBOPTIONS;i++)
		{
			SDL_BlitSurface(options[i],NULL,ecran, &rectangle);
			rectangle.y+=rectangle.h;
		}
		rectangle.x=LARGEUR_FENETRE-monPrompteur->lignes->w;
		rectangle.y=HAUTEUR_FENETRE-monPrompteur->lignes->h;
		SDL_BlitSurface(monPrompteur->lignes, NULL, ecran, &rectangle);
		SDL_Flip(ecran);
	}
	
	for(i=0;i<NBOPTIONS;i++)
	{
		SDL_FreeSurface(options[i]);
	}
	TTF_Quit();
}

int rand_a_b(int a, int b)
{
	return rand()%(b-a)+a;
}

SDL_Surface* joliBouton(int largeur, char message[20], int taillePolice, SDL_Color txtclr, Uint32 fndclr)
{
	SDL_Rect positionBouton, positionTexte;
	SDL_Surface *monBouton, *monTexte, *monBord;	
	
	monBord = SDL_CreateRGBSurface(SDL_HWSURFACE, largeur, taillePolice+5, 32, 0, 0, 0, 0);
	SDL_FillRect(monBord, NULL, SDL_MapRGB(monBord->format, 0,0,0));
	
	monBouton = SDL_CreateRGBSurface(SDL_HWSURFACE, largeur, taillePolice+3, 32, 0, 0, 0, 0);
	positionBouton.x = 2;
	positionBouton.y = 2;	
	SDL_FillRect(monBouton, NULL, fndclr);
	SDL_BlitSurface(monBouton, NULL, monBord, &positionBouton);
	
	TTF_Font* font = TTF_OpenFont(POLICE, taillePolice);	
	monTexte = TTF_RenderText_Solid(font, message, txtclr);
	positionTexte.x = 5;
	positionTexte.y = 5;
	SDL_BlitSurface(monTexte, NULL, monBord, &positionTexte);	
	
	TTF_CloseFont(font);
	
	return monBord;
}

	
Prompteur* creerPrompteur(int w,int h, SDL_Color texteCouleur, Uint32 fondCouleur)
{
	Prompteur* monPrompteur = malloc(sizeof(Prompteur));
	monPrompteur->lignes = SDL_CreateRGBSurface(SDL_HWSURFACE, w, h, 32,0,0,0,0);
	monPrompteur->texteCouleur = texteCouleur;
	monPrompteur->fondCouleur = fondCouleur;
	SDL_FillRect(monPrompteur->lignes, NULL, monPrompteur->fondCouleur);
	monPrompteur->taille = h/NBMESSAGES;
	monPrompteur->police = TTF_OpenFont(POLICE, monPrompteur->taille);
	printf("jusque là ca marche\n");
	int i;
	for(i=0;i<NBMESSAGES;i++)
	{
	monPrompteur->messages[i]= TTF_RenderText_Solid(monPrompteur->police, "", monPrompteur->texteCouleur);
	}
	printf("jusque là ca marche\n");
	return monPrompteur;
}

void libererPrompteur(Prompteur* monPrompteur)
{
	int i;
	for(i=0;i<NBMESSAGES;i++)
	{
		SDL_FreeSurface(monPrompteur->messages[i]);
	}
	SDL_FreeSurface(monPrompteur->lignes);
}

void ajouterMessage(Prompteur* monPrompteur, char message[100])
{
	SDL_FillRect(monPrompteur->lignes, NULL,  monPrompteur->fondCouleur);
	int i;
	SDL_Rect pos;
	pos.x=0;
	pos.y=0;
	for(i=NBMESSAGES-1;i>0;i--)
	{
	monPrompteur->messages[i]=monPrompteur->messages[i-1];
	SDL_BlitSurface(monPrompteur->messages[i],NULL,monPrompteur->lignes,&pos);
	pos.y+=monPrompteur->taille;
	}
	monPrompteur->messages[0] = TTF_RenderText_Solid(monPrompteur->police, message, monPrompteur->texteCouleur);
	SDL_BlitSurface(monPrompteur->messages[0],NULL,monPrompteur->lignes,&pos);
}

