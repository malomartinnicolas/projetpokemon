#ifndef MENU
#define MENU

	#include "map_constantes.h"
	#include <SDL/SDL_ttf.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include "combat.h"
	#include "save.h"
	#include "map.h"
	
	void afficherCredits(SDL_Surface *ecran);
	void afficherJouer(SDL_Surface *ecran);
	void afficherMenu(SDL_Surface *ecran);
	

#endif
