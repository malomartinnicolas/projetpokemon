#include "map.h"


void jouer(SDL_Surface * ecran,Dresseur* monDresseur, Parametres* mesParametres) 
{
	SDL_Event event;
	SDL_EnableKeyRepeat(200,200);
	int i,continuer= 1;
	int tempsActuel=0;
	int tempsPrecedent=0;
	Carte * maCarte = malloc(sizeof(Carte));
	modifierCarte(mesParametres->numeroCarte,maCarte);
	Images * mesImages = creerImages();
	modifierImages(mesParametres->numeroTheme,mesImages);
	SDL_Surface * herosActuel=mesImages->heros[maCarte->directionHeros];
	
	Monstre* monstreActuel;
	Dresseur* nemesis;
	
	afficherCarte(ecran,mesImages,herosActuel,maCarte);

	while(continuer)
	{	
		
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_QUIT:
				continuer =0;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym) 
				{
					case SDLK_DOWN:
						maCarte->directionHeros=BAS;
						break;
					case SDLK_UP:
						maCarte->directionHeros=HAUT;
						break;
					case SDLK_RIGHT:
						maCarte->directionHeros=DROITE;
						break;
					case SDLK_LEFT:
						maCarte->directionHeros=GAUCHE;	
						break;
					case SDLK_ESCAPE:
						mesParametres->fenetre = MENUU;
						break;
					default:
						break;
				}
			default:
				break;
		}
		
		tempsActuel = SDL_GetTicks();
		if (tempsActuel - tempsPrecedent > 50)
		{
		
			if ((event.key.keysym.sym==SDLK_DOWN)||(event.key.keysym.sym==SDLK_UP)||(event.key.keysym.sym==SDLK_LEFT)||(event.key.keysym.sym==SDLK_RIGHT)||(event.key.keysym.sym==SDLK_ESCAPE))
			{
			
			    	deplacer(maCarte);
		
				if (maCarte->directionHeros!=STILL)
				{
					herosActuel = mesImages->heros[maCarte->directionHeros];
				}

				monstreActuel = testerCarte(maCarte, mesImages, mesParametres, monDresseur);
				afficherCarte(ecran,mesImages,herosActuel,maCarte);
				maCarte->directionHeros=STILL;
				switch(mesParametres->fenetre)
				{
					case MAGASIN:
						menuMagasin(ecran, monDresseur);
						break;
					case AIDE1:
						message(ecran,"Par ici : le Donjon. Vos monstres combattent des monstres sauvages",56,24);
						message(ecran," et deviennent plus forts. Si vous n'en avez pas, ",56,24);
						message(ecran," allez en capturer au Safari.", 56,24);
						break;
					case AIDE2:
						message(ecran,"Par ici : le Safari. Pour capturer un monstre il faut l'affaiblir",56,24);
						message(ecran," et l'appater. A vous de trouver le juste milieu", 56,24);
						break;
					case HEAL:
						for(i=0;i<monDresseur->nb_monstres;i++)
						{
							monDresseur->equipe[i]->vie_actuelle=monDresseur->equipe[i]->vie;
						}
						message(ecran,"Vos monstres sont soignes et la difficulte reinitialisee",56,24);
						maCarte->directionHeros=HAUT;
						herosActuel = mesImages->heros[HAUT];
						deplacer(maCarte);
						break;
					case MENUU:
						continuer=menuMap(ecran, monDresseur);
						break;
					case FIGHT:
						nemesis = creerDresseur("nemesis", 0, monDresseur->niveau, mesParametres->monBestiaire, mesParametres->monAttaquaire);
						for(i=0;i<monDresseur->nb_monstres;i++)
						{
							nemesis->equipe[i]=copierMonstre(monDresseur->equipe[i]);
							nemesis->nb_monstres++;
						}
						message(ecran,"L hemorragie de tes desirs",56,24);
						message(ecran,"S est eclipsee sous la joue bleue derisoire",56,24);
						message(ecran,"Du temps qui se passe",56,24);
						message(ecran,"Contre duquel on ne peut rien",56,24);
						message(ecran,"Etre ou ne pas etre",56,24);
						message(ecran,"Telle est la question",56,24);
						message(ecran,"Sinusoidale",56,24);
						message(ecran,"De l'anachorete",56,24);
						message(ecran,"Hypochondriaque",56,24);
						if (combatDresseur(ecran, monDresseur,nemesis)!=NULL)
						{
							message(ecran,"les dev n ont pas encore trouve de suite", 56,24);
						}
						else 
						{
							message(ecran,"avec plus de skill tu aurais decouvert la magnifique fin imagine par les devs",56,24);
							mesParametres->numeroCarte = 0;
							mesParametres->numeroTheme=0;
							modifierCarte(mesParametres->numeroCarte, maCarte);
							modifierImages(mesParametres->numeroTheme,mesImages);
							herosActuel = mesImages->heros[BAS];
						}
						libererDresseur(nemesis);
						break;
					default:
						break;
					
				}
				
				afficherCarte(ecran,mesImages,herosActuel,maCarte);
				mesParametres->fenetre = RIEN;
				
				if(monstreActuel != NULL){
					apparition(ecran, monstreActuel);
					if(mesParametres->mode==DONJON)
					{
						monstreActuel = menuCombat(ecran, monDresseur, monstreActuel, 1, 0);	
						if(monstreActuel == NULL)
						{
							mesParametres->numeroCarte = 0;
							mesParametres->numeroTheme=0;
							modifierCarte(mesParametres->numeroCarte, maCarte);
							modifierImages(mesParametres->numeroTheme,mesImages);
							herosActuel = mesImages->heros[BAS];
						}
					}
					else
					{
						if(monDresseur->nb_monstres<NB_MONSTRES_MAX)
						{
							monstreActuel = ecranCapture(ecran, monstreActuel,monDresseur);
							if(monstreActuel != NULL)
							{
								ajouter(monDresseur,monstreActuel);
								miseAJourDresseur(monDresseur);
							}
						}
						else
						{
							message(ecran,"Vous avez atteint le maximum de monstre, liberez-en un",56,24);
							message(ecran,"avant d'en capturer un autre",56,24);
						}
					}					
				}
				
				afficherCarte(ecran,mesImages,herosActuel,maCarte);
			    tempsPrecedent = tempsActuel;
			}

		}
		else
		{
		    SDL_Delay(50 - (tempsActuel - tempsPrecedent));
		}
		
	}
	free(maCarte);
	libererImages(mesImages);
}

Parametres* chargerParametres(int numeroCarte,int numeroTheme, int difficulte, int mode)
{	
	Parametres* mesParametres = malloc(sizeof(Parametres));
	mesParametres->numeroCarte=numeroCarte;	
	mesParametres->numeroTheme=numeroTheme;
	mesParametres->difficulte=difficulte;
	mesParametres->mode=mode;
	mesParametres->monBestiaire = chargerBestiaire();
	mesParametres->monAttaquaire = chargerAttaquaire();
	mesParametres->fenetre = RIEN;	
	return mesParametres;
}

void libererParametres(Parametres* mesParametres)
{
	libererBestiaire(mesParametres->monBestiaire);
	libererAttaquaire(mesParametres->monAttaquaire);
	
	free(mesParametres);
}
	
	



	
	
