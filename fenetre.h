#ifndef DEF_FEN
#define DEF_FEN
	#include <stdlib.h>
	#include <stdio.h>
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <SDL/SDL_ttf.h>
	#include "map_constantes.h"
	#include "dresseur.h"
	#include "capture.h"
	#include "save.h"
	#define NMENU 5
	void message(SDL_Surface* ecran, char* m, int h, int t);
	int CparLigne(int taille, int largeur);
	int menuMap(SDL_Surface* ecran, Dresseur* monDresseur);
	void menuMonstre(SDL_Surface* ecran,SDL_Surface* ecrantmp, Dresseur* monDresseur);
	void menuobjets(SDL_Surface* ecran, SDL_Surface* ecrantmp,Dresseur* monDresseur);
	void soinMonstre(SDL_Surface* ecran, SDL_Surface* ecrantmp,Dresseur* monDresseur,int obj);
#endif
