#ifndef DEF_MAP
#define DEF_MAP
	#include "rand.h"
	#include "monstres.h"
	#include "combat.h"
	#include "map_deplacement.h"
	#include "map_affichage.h"
	#include "dresseur.h"
	#include "map_constantes.h"
	#include "save.h"
	#include "magasin.h"
	#include "menu.h"
	#include "capture.h"
	#include "fenetre.h"
	
	void jouer(SDL_Surface *ecran, Dresseur* monDresseur, Parametres* mesParametres);
	Parametres* chargerParametres(int numeroCarte,int numeroTheme, int difficulte, int mode);
	void libererParametres(Parametres*);
#endif
