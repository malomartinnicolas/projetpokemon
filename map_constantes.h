#ifndef DEF_MAP_CST
#define DEF_MAP_CST
	#include <stdlib.h>
	#include <stdio.h>
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <string.h>
	#include "monstres.h"
	#include "dresseur.h"

	#define TAILLE_CASE 48
	#define NB_CASES_LARGEUR 20
	#define NB_CASES_HAUTEUR 14
	#define LARGEUR_FENETRE TAILLE_CASE * NB_CASES_LARGEUR
	#define HAUTEUR_FENETRE TAILLE_CASE * NB_CASES_HAUTEUR
	#define NB_CARTES 8
	#define NB_THEMES 8
	#define CLR_TRSP 250,0,150
	#define POLICE "Aller_Lt.ttf"
	#define CLRTXT 255,255,255
	#define CLRFOND 255,51,0
	#define TAILLE 24
	#define CLRACTIF 51,0,51
	#define CLRPASSIF 255,51,0

	enum {HAUT, DROITE, BAS, GAUCHE, STILL};
	enum {CENTRE, SAFARI, DONJON};
	enum {RIEN,MAGASIN,AIDE1,AIDE2,HEAL,MENUU,FIGHT};                                 //fenetre à afficher en sortie de testerCarte
	typedef struct
	{
	SDL_Surface *heros[4];
	SDL_Surface *sol, *arbre, *herbe, *echelleU, *echelleD, *telep, *marchand, *heal, *panneau;
	} Images;

	typedef struct
	{
	char grille[NB_CASES_LARGEUR][NB_CASES_HAUTEUR];
	int positionHeros[2];
	int directionHeros;
	} Carte;
	
	typedef struct
	{
	int numeroCarte,numeroTheme,difficulte,mode;
	Bestiaire* monBestiaire;
	Attaquaire* monAttaquaire;
	int fenetre;
	int spawn_monstre;	
	} Parametres;
#endif

