#ifndef DEF_TEMPLATE
#define DEF_TEMPLATE	
	#include <stdlib.h>
	#include <stdio.h>
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <SDL/SDL_ttf.h>
	#include "map_constantes.h"
	#include "monstres.h"			      //optionnel pour le template
	#include "rand.h"                             //optionnel pour le template
	#define POLICE "Aller_Lt.ttf"
	#define TAILLE 24
	#define CLRACTIF 51,0,51
	#define CLRPASSIF 255,51,0
	#define CLRTXT 255,255,255
	#define NBOPTIONS 4
	#define NBMESSAGES 8

	int rand_a_b(int,int);
	SDL_Surface* joliBouton(int largeur, char message[20], int taillePolice, SDL_Color txtclr, Uint32 fndclr);
	void sousmenu(SDL_Surface* ecran, SDL_Surface* BG, SDL_Surface* HD);   //mob c'est pour afficher autour de menu, fait pas partie du template en soi
	typedef struct
	{
	SDL_Surface* lignes;
	int taille;
	TTF_Font* police;
	SDL_Surface* messages[NBMESSAGES];
	SDL_Color texteCouleur;
	Uint32 fondCouleur;
	} Prompteur;

	Prompteur* creerPrompteur(int w,int h, SDL_Color texteCouleur, Uint32 fondCouleur);
	void libererPrompteur(Prompteur* monPrompteur);
	void ajouterMessage(Prompteur* monPrompteur, char message[100]);
#endif
