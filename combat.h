#ifndef DEF_COMBAT
#define DEF_COMBAT
	#include <stdlib.h>
	#include <stdio.h>
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <SDL/SDL_ttf.h>
	#include "dresseur.h"
	#include "IA.h"
	
	void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination);
	void getVie(Monstre* monstre, char vie[15]);
	void afficherTexte(SDL_Surface* ecran, int x, int y, int w, int h, char message[50], int info);
	Monstre* menuCombat(SDL_Surface* ecran, Dresseur* monDresseur, Monstre* monstre, int nbMonstres, int dresseur);
	void menuAttaquer(SDL_Surface* ecran, Monstre* monMonstre, Monstre* monstre);
	void menuObjets(SDL_Surface* ecran, Dresseur* monDresseur, Monstre* monMonstre);
	Monstre* menuMonstres(SDL_Surface* ecran, Dresseur* monDresseur, Monstre* monMonstre, Monstre* monstre);
	void afficherMonstre2(SDL_Surface* ecran, Monstre* monMonstre);
	void afficherStats(SDL_Surface* ecran, Monstre* monMonstre, Monstre* monstre);
	Monstre* combatDresseur(SDL_Surface* ecran, Dresseur* monDresseur, Dresseur* dresseurAdverse);
#endif
