#ifndef DEF_MAP_DEP
#define DEF_MAP_DEP
	#include "map_constantes.h"
	#include "map_affichage.h"
	#include "dresseur.h"
	

	void deplacer(Carte* maCarte);

	void spawn(Carte* maCarte,char Cspawn);
	Monstre* testerCarte(Carte* maCarte, Images*, Parametres*, Dresseur*);
	Monstre* rencontre(Parametres* ,Dresseur*);
	
#endif
