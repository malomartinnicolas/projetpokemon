#include "save.c"

int rand_a_b(int a,int b)             //à inclure dans les fichiers avec des mains (donc dans les test) qui utilise des fonctions aléa (quasi toutes, 										love random )
{
	return rand()%(b-a)+a;
}

int main()
{
	srand(time(NULL));           //idem qu'au dessus
	int i = 0;
	
	Dresseur* dresseur = creerDresseur("Chocolat", 3, 0);
	save(dresseur, 1);
	Dresseur* dresseur2 = creerDresseur("Sacha", 6, 5);
	save(dresseur2, 2);

	Dresseur* monDresseur = load(2);

	printf("%s %d %d\n", monDresseur->nom, monDresseur->nb_monstres, monDresseur->niveau);
	
	for(i=0; i<monDresseur ->nb_monstres;i++)
	{
		printf("%s : %d %d %d %d\n", monDresseur->equipe[i]->nom, monDresseur->equipe[i]->niveau, monDresseur->equipe[i]->vie, monDresseur->equipe[i]->force, monDresseur->equipe[i]->habilete);
	}
	
	printf("%s : %d %d %d %d\n", monDresseur->equipe[0]->attaque[0]->nom, monDresseur->equipe[0]->attaque[0]->indice_rarete, monDresseur->equipe[0]->attaque[0]->puissance, monDresseur->equipe[0]->attaque[0]->heal, monDresseur->equipe[0]->attaque[0]->precision);

	return EXIT_SUCCESS;
}