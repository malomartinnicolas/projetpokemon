#include "map_affichage.h"

int x,y,i;

Images* creerImages()
{
	Images* mesImages=malloc(sizeof(Images));
	
	mesImages->sol = SDL_LoadBMP("./Images/theme0/sol.bmp");
	mesImages->arbre = SDL_LoadBMP("./Images/theme0/arbre.bmp");
	mesImages->herbe = SDL_LoadBMP("./Images/theme0/herbe.bmp");
	
	mesImages->marchand = SDL_LoadBMP("./Images/Dresseurs/marchand/marchand.bmp");
	SDL_SetColorKey(mesImages->marchand, SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->marchand->format, 94,66,41));

	mesImages->echelleU = SDL_LoadBMP("./Images/echelle_u.bmp");
	SDL_SetColorKey(mesImages->echelleU, SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->echelleU->format, CLR_TRSP));

	mesImages->echelleD = SDL_LoadBMP("./Images/echelle_d.bmp");
	SDL_SetColorKey(mesImages->echelleD, SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->echelleD->format, CLR_TRSP));

	mesImages->telep = SDL_LoadBMP("./Images/tp.bmp");
	SDL_SetColorKey(mesImages->telep, SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->telep->format, CLR_TRSP));
	
	mesImages->heal = SDL_LoadBMP("./Images/heal.bmp");
	SDL_SetColorKey(mesImages->heal, SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->heal->format, CLR_TRSP));
	
	mesImages->panneau = SDL_LoadBMP("./Images/panneau.bmp");
	SDL_SetColorKey(mesImages->panneau, SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->panneau->format, CLR_TRSP));

	mesImages->heros[HAUT] = SDL_LoadBMP("./Images/Dresseurs/heros/s_up.bmp");
	SDL_SetColorKey(mesImages->heros[HAUT], SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->heros[HAUT]->format, CLR_TRSP));
	
	mesImages->heros[DROITE] = SDL_LoadBMP("./Images/Dresseurs/heros/s_right.bmp");
	SDL_SetColorKey(mesImages->heros[DROITE], SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->heros[DROITE]->format, CLR_TRSP));
	
	mesImages->heros[BAS] = SDL_LoadBMP("./Images/Dresseurs/heros/s_down.bmp");
	SDL_SetColorKey(mesImages->heros[BAS], SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->heros[BAS]->format, CLR_TRSP));
	
	mesImages->heros[GAUCHE] = SDL_LoadBMP("./Images/Dresseurs/heros/s_left.bmp");
	SDL_SetColorKey(mesImages->heros[GAUCHE], SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->heros[GAUCHE]->format, CLR_TRSP));
	return mesImages;
}

void modifierImages(int i, Images* mesImages)
{
	char Csol[30];
	sprintf(Csol,"./Images/theme%d/sol.bmp",i);
	SDL_FreeSurface(mesImages->sol);
	mesImages->sol = SDL_LoadBMP(Csol);

	char Carbre[30];
	sprintf(Carbre,"./Images/theme%d/arbre.bmp",i);
	SDL_FreeSurface(mesImages->arbre);
	mesImages->arbre = SDL_LoadBMP(Carbre);
	SDL_SetColorKey(mesImages->arbre, SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->arbre->format, CLR_TRSP));

	char Cherbe[30];
	sprintf(Cherbe,"./Images/theme%d/herbe.bmp",i);
	SDL_FreeSurface(mesImages->herbe);
	mesImages->herbe = SDL_LoadBMP(Cherbe);
	SDL_SetColorKey(mesImages->herbe, SDL_SRCCOLORKEY, SDL_MapRGB(mesImages->herbe->format, CLR_TRSP));
}

void libererImages(Images* mesImages)
{
	SDL_FreeSurface(mesImages->sol);
	SDL_FreeSurface(mesImages->arbre);
	SDL_FreeSurface(mesImages->herbe);
	SDL_FreeSurface(mesImages->echelleU);
	SDL_FreeSurface(mesImages->echelleD);
	SDL_FreeSurface(mesImages->telep);
	SDL_FreeSurface(mesImages->marchand);
	SDL_FreeSurface(mesImages->heal);
	SDL_FreeSurface(mesImages->panneau);
	for(i=0;i<4;i++)
	{
		SDL_FreeSurface(mesImages->heros[i]);
	}
	free(mesImages);
}
	
void modifierCarte(int n, Carte* maCarte) 
{
	char Cmap[30];
	char tmp='0';
	sprintf(Cmap,"./Maps/map%d.map", n);
	FILE * map = fopen(Cmap,"r");
	i=0;
	do 
	{
		tmp=fgetc(map);
		if (tmp!='\n')
		{	
			maCarte->grille[i%NB_CASES_LARGEUR][i/NB_CASES_LARGEUR]=tmp;
			i++;
		}
		
	}
	while((tmp!=EOF)&&(i<NB_CASES_LARGEUR * NB_CASES_HAUTEUR));
	maCarte->directionHeros=BAS;
	spawn(maCarte, 'I');
	fclose(map);
}

	

void afficherCarte(SDL_Surface* ecran,Images* mesImages,SDL_Surface* herosActuel,Carte* maCarte)
{
	SDL_Rect position;
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
	for (x=0;x<NB_CASES_LARGEUR;x++)
	{
		for (y=0;y<NB_CASES_HAUTEUR;y++)
		{
			position.x=TAILLE_CASE*x;
			position.y=TAILLE_CASE*y;
			SDL_BlitSurface(mesImages->sol, NULL, ecran,&position);
			switch(maCarte->grille[x][y])
			{
				case 'A':
					SDL_BlitSurface(mesImages->arbre, NULL, ecran,&position);
					break;
				case 'H':
					SDL_BlitSurface(mesImages->herbe, NULL, ecran,&position);
					break;
				case '-':
					SDL_BlitSurface(mesImages->echelleD,NULL, ecran, &position);
					break;
				case '+':
					SDL_BlitSurface(mesImages->echelleU,NULL, ecran, &position);
					break;
				case 'C':
					SDL_BlitSurface(mesImages->telep,NULL, ecran, &position);
					break;
				case 'S':
					SDL_BlitSurface(mesImages->telep,NULL, ecran, &position);
					break;
				case 'D':
					SDL_BlitSurface(mesImages->telep,NULL, ecran, &position);
					break;
				case 'M':
					SDL_BlitSurface(mesImages->marchand, NULL, ecran, &position);
					break;
				case 'R':
					SDL_BlitSurface(mesImages->heal,NULL,ecran,&position);
					break;
				case 'P':
					SDL_BlitSurface(mesImages->panneau, NULL, ecran, &position);
					break;
				case 'K':
					SDL_BlitSurface(mesImages->heros[BAS], NULL, ecran, &position);
					break;
								
			}
			if ((x==maCarte->positionHeros[0])&&(y==maCarte->positionHeros[1]))
			{
				SDL_BlitSurface(herosActuel, NULL, ecran, &position);
			}
		}
	}
	SDL_Flip(ecran);
}


void apparition(SDL_Surface* ecran, Monstre* unMonstre)
{	
	int x;
	SDL_Event event;
	SDL_Rect position;
	SDL_Rect positionB;
	position.y=HAUTEUR_FENETRE/2-MOB_HAUTEUR/2;
	positionB.y=HAUTEUR_FENETRE/2-MOB_HAUTEUR/2;
	positionB.x=0;
	positionB.h= 1.1*MOB_HAUTEUR;
	positionB.w= LARGEUR_FENETRE;
	for (x=0;x<LARGEUR_FENETRE;x+=4)
	{
		SDL_PollEvent(&event);
		SDL_FillRect(ecran, &positionB, SDL_MapRGB(ecran->format, 0, 0, 0));
		position.x=x;
		SDL_BlitSurface(unMonstre->front,NULL,ecran,&position);
		SDL_Flip(ecran);
	}
}
		
	
			







