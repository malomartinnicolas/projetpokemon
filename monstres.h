#ifndef DEF_MST
#define DEF_MST
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <dirent.h>
	#include <math.h>
	#include "rand.h"
	
	enum {NORMAL,EAU,FEU,PLANTE};
	#define CLR 106,76,48
	#define MOB_LARGEUR 192
	#define MOB_HAUTEUR 192

	#define NB_MOB_TYPE(type) ((type==FEU)?5:\
				((type==EAU)?4:\
				((type==PLANTE)?4:\
				((type==NORMAL)?9:0))))
	#define NB_MOB NB_MOB_TYPE(FEU)+NB_MOB_TYPE(EAU)+NB_MOB_TYPE(PLANTE)+NB_MOB_TYPE(NORMAL)

	#define NB_ATT_TYPE(type) ((type==FEU)?5:\
				((type==EAU)?6:\
				((type==PLANTE)?5:\
				((type==NORMAL)?5:0))))
	#define NB_ATT NB_ATT_TYPE(FEU)+NB_ATT_TYPE(EAU)+NB_ATT_TYPE(PLANTE)+NB_ATT_TYPE(NORMAL)

	#define TYPESTRING(type) ((type==FEU)?"FEU":\
				((type==EAU)?"EAU":\
				((type==PLANTE)?"PLANTE":\
				((type==NORMAL)?"NORMAL":""))))
	
	typedef struct
	{
	char nom[15];
	int puissance, heal,precision,type;
	int indice_rarete;
	} Attaque;

	typedef struct
	{
	Attaque** attaques[4];
	} Attaquaire; 


	typedef struct
	{
	char nom[20];
	SDL_Surface *front,*back;
	int niveau, vie, force, habilete,type, vie_actuelle, experience;
	int indice_rarete;       //influe sur le % d'apparition, la puissance des attaques possibles. Logiquement bonnes stats/mob stylé/et le 					dit indice élevé vont ensembles mais pas forcement. indice = 100 => mob qui claque. indice = 10 => mob de merde.   
	Attaque* attaque[4];
	} Monstre;
	
	typedef struct
	{
	Monstre** monstres[4];
	} Bestiaire; 
	
	
	Attaque* chargerAttaque(char nom[15], int type);
	//Pas besoin de fonction liberer, la structure ne contient pas de pointeur. Un free suffit.

	Attaque** chargerAttaquesType(int type);
	void libererAttaquesType(Attaque**, int type);

	Attaquaire* chargerAttaquaire();
	void libererAttaquaire(Attaquaire* monAttaquaire);

	Monstre* chargerMonstre(char nom[20], int type);
	void libererMonstre(Monstre*);
	
	Monstre** chargerMonstresType(int type);
	void libererMonstresType(Monstre**, int type);
	
	Bestiaire* chargerBestiaire();
	void libererBestiaire(Bestiaire* monBestiaire);

	
	Attaque* chargerAttaqueAlea(int type, Attaquaire* monAttaquaire);
	void genererAttaqueAlea(Monstre* monMonstre, Attaquaire* monAttaquaire);

	void levelUp(Monstre*);
	Monstre* copierMonstre(Monstre*);

	Monstre* genererMonstre(int type,int niveau,Bestiaire*, Attaquaire*);
	
	void chargerImagesMonstre(Monstre* monMonstre);
	
#endif
