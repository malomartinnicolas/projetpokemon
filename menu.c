#include "menu.h"

#define TAILLEPOLICE 24

const SDL_Color couleurTexte = {255,255,255};
const SDL_Color orange = {251,51,0};

void afficherCredits(SDL_Surface *ecran)
{	
	SDL_Surface *imageDeFondCredits = NULL;
	imageDeFondCredits = IMG_Load("Images/Acceuil/imageCredits.png"); //choix de l'image de credits
	
	SDL_Rect positionFond; //position de l'image de fond
   	  positionFond.x = 0;
    	positionFond.y = 0;
    	
    	SDL_BlitSurface(imageDeFondCredits, NULL, ecran, &positionFond);
    	SDL_Flip(ecran);
    	
    	SDL_Event event;
    	int poursuivre = 1;
    	while (poursuivre)
    	{
    		SDL_WaitEvent(&event);
    		switch(event.type)
    		{
    			case SDL_QUIT:
    				poursuivre=0;
    				break;
    			case SDL_KEYDOWN:
    				switch(event.key.keysym.sym)
    				{
    					case SDLK_ESCAPE:
    						poursuivre=0;
    						break;
    						
    					case SDLK_RETURN:
    						poursuivre=0;
    						break;
    				}
    		}
    	}
    	SDL_FreeSurface(imageDeFondCredits);	
}	

void afficherJouer(SDL_Surface *ecran)
{
    int compteur=0;
    
    Parametres* mesParametres = chargerParametres(0,0,0,CENTRE);
    Dresseur* monDresseur = creerDresseur("Sacha",0,0,mesParametres->monBestiaire, mesParametres->monAttaquaire);
    SDL_EnableKeyRepeat(100, 200);
    SDL_Event event; //variable d'événement
        
    SDL_Surface *imageDeFond = NULL, *bouton_actif = NULL, *bouton_passif = NULL, *bouton_save1 = NULL, *bouton_save2 = NULL, *bouton_save3 = NULL, *monTexte = NULL;
     
    imageDeFond = IMG_Load("Images/Acceuil/imageFond.png");  //choix de l'image de fond
    bouton_actif = IMG_Load("Images/Acceuil/bouton_passif.png"); //choix de l'image du bouton actif
    bouton_passif = IMG_Load("Images/Acceuil/bouton_actif.png");  //choix de l'image du bouton passif
    
    TTF_Font *font = NULL;
    font = TTF_OpenFont("Aller_Lt.ttf", TAILLEPOLICE);
    bouton_save1 = TTF_RenderText_Solid(font, "Partie 1", couleurTexte);
    bouton_save2 = TTF_RenderText_Solid(font, "Partie 2", couleurTexte);
    bouton_save3 = TTF_RenderText_Solid(font, "Partie 3", couleurTexte); 
    
    TTF_Font *font2 = NULL;
    font2 = TTF_OpenFont("Aller_Lt.ttf", 25);	
    monTexte = TTF_RenderText_Solid(font2, "Charger Partie : Appuyer sur ENTER, Ecraser Partie : Appuyer sur BACKSPACE", orange);
        
    SDL_Rect positionFond; //position de l'image de fond
    positionFond.x = 0;
    positionFond.y = 0;
    
    SDL_Rect position1; //position du bouton save1
    position1.x = ecran->w / 2 - bouton_actif->w / 2;
    position1.y = 150; 
    
    SDL_Rect position2; //position du bouton save2
    position2.x = ecran->w / 2 - bouton_actif->w / 2;
    position2.y = 300;
    
    SDL_Rect position3; //position du bouton save3
    position3.x = ecran->w / 2 - bouton_actif->w / 2;
    position3.y = 450;	
    
    SDL_Rect positionTexte; //position du texte
    positionTexte.x = 50;
    positionTexte.y = ecran->h - 50;
        
    SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);  //mise en place de l'image de fond
    SDL_BlitSurface(bouton_actif, NULL, ecran, &position1); //mise en place des boutons 
    apply_surface(ecran->w / 2 - bouton_save1->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_save1, ecran);  
    SDL_BlitSurface(bouton_passif, NULL, ecran, &position2);
    apply_surface(ecran->w / 2 - bouton_save2->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_save2, ecran);
    SDL_BlitSurface(bouton_passif, NULL, ecran, &position3);
    apply_surface(ecran->w / 2 - bouton_save3->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_save3, ecran);
    SDL_BlitSurface(monTexte, NULL, ecran, &positionTexte);
    
    SDL_Flip(ecran); //mise à jour de l'écran
    
    int poursuivre = 1;
    while (poursuivre)
    {
    	SDL_WaitEvent(&event);
    	switch(event.type)
    	{
    		case SDL_QUIT:
    			poursuivre = 0;
    			break;
    		case SDL_KEYDOWN:
    			switch(compteur)
    			{	
    				case 0:
    					switch(event.key.keysym.sym)
    					{	
    						case SDLK_ESCAPE:
    							poursuivre=0;
    							break;
    						case SDLK_BACKSPACE: //à modifier pour lancer une nouvelle partie sur la sauvegarde 1
    							monDresseur->save=1;
    						    	jouer(ecran, monDresseur, mesParametres);
    						    	poursuivre=0;
    							break;
    						case SDLK_RETURN: //à modifier pour lancer la sauvegarde 1
    							free(monDresseur);
    							monDresseur = load(1);
    							jouer(ecran,monDresseur,mesParametres);
    							poursuivre=0;
    							break;
    						case SDLK_DOWN:
    							compteur=(compteur+4)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(monTexte, NULL, ecran, &positionTexte);
   							SDL_BlitSurface(bouton_passif, NULL, ecran, &position1);
   							apply_surface(ecran->w / 2 - bouton_save1->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_save1, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &position2);
    							apply_surface(ecran->w / 2 - bouton_save2->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_save2, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position3);
    							apply_surface(ecran->w / 2 - bouton_save3->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_save3, ecran);    
    							SDL_Flip(ecran);    								
    							break;	
    						case SDLK_UP:
    							compteur=(compteur+2)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(monTexte, NULL, ecran, &positionTexte);    								SDL_BlitSurface(bouton_passif, NULL, ecran, &position1);
   							apply_surface(ecran->w / 2 - bouton_save1->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_save1, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &position3);
    							apply_surface(ecran->w / 2 - bouton_save3->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_save3, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position2);
    							apply_surface(ecran->w / 2 - bouton_save2->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_save2, ecran);    
    							SDL_Flip(ecran);    								
    							break;				
       					}
    					break;
    				case 1:
    					switch(event.key.keysym.sym)
    					{
    						case SDLK_ESCAPE:
    							poursuivre=0;
    							break;    						
    						case SDLK_BACKSPACE: //à modifier pour lancer une nouvelle partie sur la sauvegarde 2
    							monDresseur->save=2;
    						    	jouer(ecran,monDresseur,mesParametres);
    						    	poursuivre=0;
    							break;						
    						case SDLK_RETURN: //à modifier pour lancer la sauvegarde 2
    							free(monDresseur);
    							monDresseur = load(2);
    							jouer(ecran,monDresseur,mesParametres);
    							poursuivre=0;
    							break;   							
    						case SDLK_DOWN:
    							compteur=(compteur+4)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(monTexte, NULL, ecran, &positionTexte);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position2);
    							apply_surface(ecran->w / 2 - bouton_save2->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_save2, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &position3);
    							apply_surface(ecran->w / 2 - bouton_save3->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_save3, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position1);
   							apply_surface(ecran->w / 2 - bouton_save1->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_save1, ecran);    
    							SDL_Flip(ecran);    								
    							break;	
    						case SDLK_UP:
   							compteur=(compteur+2)%3;
   							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
   							SDL_BlitSurface(monTexte, NULL, ecran, &positionTexte);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position2);
    							apply_surface(ecran->w / 2 - bouton_save2->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_save2, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &position1);
    							apply_surface(ecran->w / 2 - bouton_save1->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_save1, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position3);
    							apply_surface(ecran->w / 2 - bouton_save3->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_save3, ecran);    
    							SDL_Flip(ecran);    								
    							break;				
       					}
    					break;
    				case 2:
    					switch(event.key.keysym.sym)
    					{	
    						case SDLK_ESCAPE:
    							poursuivre=0;
    							break;
    						case SDLK_BACKSPACE: //à modifier pour lancer une nouvelle partie sur la sauvegarde 3
    							monDresseur->save=3;
    						    	jouer(ecran,monDresseur, mesParametres);
    						    	poursuivre=0;
    							break;
    						case SDLK_RETURN: //à modifier pour lancer la sauvegarde 3
    							monDresseur = load(3);
    							jouer(ecran,monDresseur, mesParametres);
    							poursuivre=0;
    							break;
    						case SDLK_DOWN: 
    							compteur=(compteur+4)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(monTexte, NULL, ecran, &positionTexte);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position3);
    							apply_surface(ecran->w / 2 - bouton_save3->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_save3, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &position1);
    							apply_surface(ecran->w / 2 - bouton_save1->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_save1, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position2);
    							apply_surface(ecran->w / 2 - bouton_save2->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_save2, ecran);    
    							SDL_Flip(ecran);    								
    							break;	
    						case SDLK_UP:
    							compteur=(compteur+2)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(monTexte, NULL, ecran, &positionTexte);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &position3);
    							apply_surface(ecran->w / 2 - bouton_save3->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_save3, ecran);    						    
 							SDL_BlitSurface(bouton_actif, NULL, ecran, &position2);
 							apply_surface(ecran->w / 2 - bouton_save2->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_save2, ecran);
 							SDL_BlitSurface(bouton_passif, NULL, ecran, &position1);
   							apply_surface(ecran->w / 2 - bouton_save1->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_save1, ecran);    
    							SDL_Flip(ecran);    								
    							break;				
       					}
    					break;
    				
    					
    				
    			}
    
    	}
    }
    

    libererParametres(mesParametres);

    SDL_FreeSurface(imageDeFond); //free des images utilisées
    SDL_FreeSurface(bouton_actif);
    SDL_FreeSurface(bouton_passif);
    SDL_FreeSurface(monTexte);
    SDL_FreeSurface(bouton_save1);
    SDL_FreeSurface(bouton_save2);
    SDL_FreeSurface(bouton_save3);
    TTF_CloseFont(font);
    TTF_CloseFont(font2);
	
}  

void afficherMenu(SDL_Surface *ecran)
{
    int continuer=1;
    int compteur=0;
    
    SDL_EnableKeyRepeat(100, 200);
    SDL_Event event; //variable d'événement
        
    SDL_Surface *imageDeFond = NULL, *bouton_actif = NULL, *bouton_passif = NULL, *bouton_jouer = NULL, *bouton_credits = NULL, *bouton_quitter = NULL;
    
    SDL_WM_SetIcon(SDL_LoadBMP("Images/Dresseurs/heros/s_down.bmp"), NULL); //modification de l'icone
    
    
    SDL_WM_SetCaption("C'est parti !", NULL); //texte de la barre d'en haut
    
    imageDeFond = IMG_Load("Images/Acceuil/imageFond.png");  //choix de l'image de fond
    bouton_actif = IMG_Load("Images/Acceuil/bouton_passif.png"); //choix de l'image du bouton actif
    bouton_passif = IMG_Load("Images/Acceuil/bouton_actif.png");  //choix de l'image du bouton passif
    
    TTF_Font *font = TTF_OpenFont("Aller_Lt.ttf", TAILLEPOLICE);
    bouton_jouer = TTF_RenderText_Solid(font, "Jouer", couleurTexte);
    bouton_credits = TTF_RenderText_Solid(font, "Credits", couleurTexte);
    bouton_quitter = TTF_RenderText_Solid(font, "Quitter", couleurTexte); 
        
    SDL_Rect positionFond; //position de l'image de fond
    positionFond.x = 0;
    positionFond.y = 0;
    
    SDL_Rect positionJ; //position du bouton jouer
    positionJ.x = ecran->w / 2 - bouton_actif->w / 2;
    positionJ.y = 150; 
    
    SDL_Rect positionC; //position du bouton credits
    positionC.x = ecran->w / 2 - bouton_actif->w / 2;
    positionC.y = 300;
    
    SDL_Rect positionQ; //position du bouton quitter
    positionQ.x = ecran->w / 2 - bouton_actif->w / 2;
    positionQ.y = 450;	
        
    SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);  //mise en place de l'image de fond
    SDL_BlitSurface(bouton_actif, NULL, ecran, &positionJ); //mise en place des boutons 
    apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);  
    SDL_BlitSurface(bouton_passif, NULL, ecran, &positionC);
    apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);
    SDL_BlitSurface(bouton_passif, NULL, ecran, &positionQ);
    apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);
    
    SDL_Flip(ecran); //mise à jour de l'écran
    
    while (continuer)
    {
    	SDL_WaitEvent(&event);
    	switch(event.type)
    	{
    		case SDL_QUIT:
    			continuer = 0;
    			break;
    		case SDL_KEYDOWN:
    			switch(compteur)
    			{	
    				case 0:
    					switch(event.key.keysym.sym)
    					{
    						case SDLK_RETURN: //entrer dans le menu Jouer
    							afficherJouer(ecran);
    							compteur=0;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
   							SDL_BlitSurface(bouton_actif, NULL, ecran, &positionJ);
   							apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);    
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionC);
    							apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionQ);
    							apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);    
    							SDL_Flip(ecran);
    							break;   	
    						case SDLK_DOWN:
    							compteur=(compteur+4)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
   							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionJ);
   							apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &positionC);
    							apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionQ);
    							apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);    
    							SDL_Flip(ecran);    								
    							break;	
    						case SDLK_UP:
    							compteur=(compteur+2)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);    								SDL_BlitSurface(bouton_passif, NULL, ecran, &positionJ);
   							apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &positionQ);
    							apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionC);
    							apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);    
    							SDL_Flip(ecran);    								
    							break;	
    							
    						case SDLK_ESCAPE:
    							continuer = 0;
    							break;			
       					}
    					break;
    				case 1:
    					switch(event.key.keysym.sym)
    					{
    						case SDLK_RETURN: //à modifier pour afficher les crédits
    							afficherCredits(ecran);
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
   							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionJ);
   							apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &positionC);
    							apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionQ);
    							apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);    
    							SDL_Flip(ecran);
    							break;   							
    						case SDLK_DOWN:
    							compteur=(compteur+4)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionC);
    							apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &positionQ);
    							apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionJ);
   							apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);    
    							SDL_Flip(ecran);    								
    							break;	
    						case SDLK_UP:
   							compteur=(compteur+2)%3;
   							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionC);
    							apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &positionJ);
    							apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionQ);
    							apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);    
    							SDL_Flip(ecran);    								
    							break;	
    						
    						case SDLK_ESCAPE:
    							continuer = 0;
    							break;					
       					}
    					break;
    				case 2:
    					switch(event.key.keysym.sym)
    					{
    						case SDLK_RETURN:
    							continuer=0;
    							break;
    						case SDLK_DOWN:
    							compteur=(compteur+4)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionQ);
    							apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);    
    							SDL_BlitSurface(bouton_actif, NULL, ecran, &positionJ);
    							apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionC);
    							apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);    
    							SDL_Flip(ecran);    								
    							break;	
    						case SDLK_UP:
    							compteur=(compteur+2)%3;
    							SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);
    							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionQ);
    							apply_surface(ecran->w / 2 - bouton_quitter->w / 2,450 + bouton_actif->h / 2 - 15 , bouton_quitter, ecran);    						    
 							SDL_BlitSurface(bouton_actif, NULL, ecran, &positionC);
 							apply_surface(ecran->w / 2 - bouton_credits->w / 2,300 + bouton_actif->h / 2 - 15 , bouton_credits, ecran);
 							SDL_BlitSurface(bouton_passif, NULL, ecran, &positionJ);
   							apply_surface(ecran->w / 2 - bouton_jouer->w / 2,150 + bouton_actif->h / 2 - 15 , bouton_jouer, ecran);    
    							SDL_Flip(ecran);    								
    							break;		
    							
    						case SDLK_ESCAPE:
    							continuer = 0;
    							break;				
       					}
    					break;
    				
    					
    				
    			}
    
    	}
    }
    
    SDL_FreeSurface(imageDeFond); //free des images utilisées
    SDL_FreeSurface(bouton_actif);
    SDL_FreeSurface(bouton_passif);
    SDL_FreeSurface(bouton_jouer);
    SDL_FreeSurface(bouton_credits);
    SDL_FreeSurface(bouton_quitter);
    TTF_CloseFont(font);
 
}  


